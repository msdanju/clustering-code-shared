#include <iostream>
#include <unordered_map>
#include "BasicClusters.h"
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <chrono>
#include <set>
#include <vector>
#include <algorithm>
#include <omp.h>
#include <unordered_map>

using namespace std;
BasicClusters C;
int numOfPoints = 0, finalNumOfClusters, n = 0;
ofstream outFile ;
string inFileName, fileName;
int thresholdDistance;
vector<vector<Node*>*> dataPoints; // vector[i] contains a pointer to a vector of pointers to Nodes for all points of the form (x,i)
int max_X, max_Y;
int numOfThreads;
string ROOT_DIR;
string resultFileName;
vector<vector<int>> nbrCountsPerThread;
vector<int> globalNbrCountVector;
unordered_map<int, int> mp;

double findDistance(int x1, int y1, int x2, int y2)
{
		return sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
}


void writeInputForStaticClus()
{
		string finalFileName;
		finalFileName.append(ROOT_DIR).append("/static/inputsForStaticClustering/").append("newData_").append(fileName);
		ofstream f1;
		f1.open(finalFileName, ios::trunc | ios::out);	
		f1 << numOfPoints << "\t" << max_X << "\t" << max_Y << endl; // first line of the file
		vector<Node*> listPerYval;
		vector<Node*>::iterator itr;
		Node* currentPoint;
		for(int i = 1; i <= max_Y; i++)
		{
				listPerYval = (*dataPoints[i]);
				for(itr = listPerYval.begin(); itr != listPerYval.end(); ++itr)
				{
						currentPoint = *itr;
						f1 << currentPoint->x << "\t" << currentPoint->y  << endl;
				}
		}	
		f1.close();
}

void writeNbrCountOutput()
{
		const int dir1 = system("mkdir -p results");
		if(dir1 < 0)
		{
				cout << "results directory could not be created.. Exiting.." << endl;
				exit(0);
		}
		string fname;
		fname.append("results/").append(resultFileName); 
    outFile.open(fname, ios::trunc | ios::out);
		for(auto x : mp)
			outFile << x.first << "\t" << x.second << endl; // first line of the file
		outFile.close();
}

bool compareNodesOnX(const Node* n1, const Node* n2)
{
		return n1->x < n2->x;
}



int countNbrsWithinThreshold(vector<Node*> &listToBeSearched, Node* currentPoint, int x, int y, int xStart, int xEnd)
{
	vector<Node*>::iterator nextListItr;
    Node* nextPoint;
    double distance;
		int count = 0;
    for(nextListItr = listToBeSearched.begin(); nextListItr != listToBeSearched.end(); ++nextListItr)
    {
        nextPoint = *nextListItr;
        if(nextPoint->x < xStart)
            continue;
        if(nextPoint->x > xEnd)
            break;
				else
        {
            distance = findDistance(x, y, nextPoint->x, nextPoint->y);
            if(distance <= thresholdDistance && distance > 0.0)
							count++;
				}					
		}	
		return count;
}

void findDegreeDistribution()
{
		#pragma omp parallel for num_threads(numOfThreads) 
		for(int y_level = 1; y_level <= max_Y; y_level++)
		{
				int tid;
#ifdef _OPENMP
				tid = omp_get_thread_num();
#else
				tid = 0;
#endif
				vector<Node*> listPerYval;
				vector<Node*>::iterator itr;

				listPerYval = (*dataPoints[y_level]);
				for(itr = listPerYval.begin(); itr != listPerYval.end(); ++itr)
				{
						Node *currentPoint = *itr;
						int x, y, xStart, xEnd, yStart, yEnd;
						x = currentPoint->x;
						y =  currentPoint->y;
						yStart = ((y - thresholdDistance) < 1) ? 1 : (y - thresholdDistance);
						yEnd = ((y + thresholdDistance) > max_Y ) ? max_Y : (y + thresholdDistance);
						xStart = ((x - thresholdDistance) < 1) ? 1 : (x - thresholdDistance);
						xEnd = ((x + thresholdDistance) > max_X ) ? max_X : (x + thresholdDistance);
						vector<Node*> listToBeSearched;
						int count = 0;
						// run this loop in parallel. let threads write the number of nbrs within threshold for each point into a vector's different indexes. Finally, we can take a frequency count of each value.
						for(int i = yStart; i <= yEnd; i++) // Go over each list in the vector dataPoints at index i.
						{
								listToBeSearched = (*dataPoints[i]);
								count += countNbrsWithinThreshold(listToBeSearched, currentPoint, x, y, xStart, xEnd);
						}

						nbrCountsPerThread[tid].push_back(count);

				}
		}
	// GO over nbrCountsPerThread and put all that to a single vector.
	// then take the frequency of each nbrCount value.

	for(int i = 0; i < numOfThreads; i++)
	{
		for (int j = 0; j < nbrCountsPerThread[i].size(); j++)
		{
			globalNbrCountVector.push_back(nbrCountsPerThread[i][j]);
		}
	}
	sort(globalNbrCountVector.begin(), globalNbrCountVector.end());
	for(int i = 0; i < n; i++)
		mp[globalNbrCountVector[i]]++;
				
}


int main(int argc, char* argv[])
{
		if(argc != 4)
		{
				cout << "Usage: " << argv[0] << " <INPUT_FILE_NAME> <THRESHOLD_DISTANCE>  <NUM_THREADS>" << endl;
				exit(0);
		}
		ROOT_DIR = getenv("CLUSTERING_ROOT");
		fileName = argv[1];
		thresholdDistance = atoi(argv[2]);
		string dynDataFileName;
		numOfThreads = atoi(argv[3]);
		string inFilename;
		inFilename.append(ROOT_DIR).append("/inputs/").append(fileName);
		resultFileName.append("nbrCount_thresh_").append(argv[2]).append("_").append(fileName);
		int x, y, label;
		Node* currentPoint;
		fstream inputFile;
		inputFile.open(inFilename, ios_base::in);
		if(!(inputFile.is_open()))
		{
				cout << "Could not open the file " << inFilename << ".. Exiting program..." << endl;
				exit(0);
		}
		/* Read the coordinates of the input points  */
		inputFile >> n; // Reading the number of points in this input file into global variable n.
		inputFile >> max_X;
		inputFile >> max_Y;
		// n points are there in the input file. And we are going to add 1000 points. But these 1000 points will have max values of X and Y coordinates max_X and max_Y respectively. So the vector size is reserved accordingly.

		dataPoints.reserve(max_Y + 1); /* +1 so that indexing from 1 can be used */
		nbrCountsPerThread.reserve(numOfThreads);
		nbrCountsPerThread.resize(numOfThreads);
		set<int> uniqueLabels;
		// remember to deallocate these vectors
		for(int i = 1; i <= max_Y; i++)
		{
				dataPoints[i] = new vector<Node*>;
		}
		while(true)
		{
				if(!(inputFile >> x))
						break;
				inputFile >> y;
				inputFile >> label;
				numOfPoints++;
				currentPoint = C.makeSingleElemCluster(x, y, label);
				(*dataPoints[y]).push_back(currentPoint);
		}
		// The vectors are already in sorted order. - ASSUMPTION
		inputFile.close();

		findDegreeDistribution();
		writeNbrCountOutput();
}
