#include <iostream>
#include <unordered_map>
#include "BasicClusters.h"
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <chrono>
#include <set>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <omp.h>
using namespace std;

BasicClusters C;
int numOfPoints = 0, finalNumOfClusters;
int LARGE= 33554432; // =2^25i
ofstream outFile, allResultsFile;
std::chrono::steady_clock::time_point a, b, c, d, e, f;
float clusteringTime, totalExecTime, inputReadTime, writeOutTime;
string inFileName, fileName;
int thresholdDistance;
vector<vector<Node*>*> dataPoints; // vector[i] contains a pointer to a vector of pointers to Nodes for all points of the form (x,i)
int max_X, max_Y;
vector<set<pair<Label*, Label*>>> labelPairsPerThread; // vector of sets
int numOfThreads;
string ROOT_DIR;

double findDistance(int x1, int y1, int x2, int y2)
{
	return sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
}

/* This function merges the label of currentPoint with the labels of all the nbrs in a particular y line(listToBeSearched) */
void mergeWithAllNbrs(vector<Node*> &listToBeSearched, Node* currentPoint, int x, int y, int xStart, int xEnd)
{
    vector<Node*>::iterator nextListItr;
    Node* nextPoint;
    double distance;
    Label *parentLabel_a, *parentLabel_b;
    int tid;
#ifdef _OPENMP
    tid = omp_get_thread_num();
#else
    tid = 0;
#endif

    for(nextListItr = listToBeSearched.begin(); nextListItr != listToBeSearched.end(); ++nextListItr)
    {
        nextPoint = *nextListItr;
        if(nextPoint->x < xStart)
            continue;
        if(nextPoint->x > xEnd)
            break;
        else
        {
            distance = findDistance(x, y, nextPoint->x, nextPoint->y);
            if(distance <= thresholdDistance)
            {
                parentLabel_a = C.findRootOfCluster(currentPoint->ptrToLabel);
                parentLabel_b = C.findRootOfCluster(nextPoint->ptrToLabel);
                if(parentLabel_a != parentLabel_b)
                {
                    labelPairsPerThread[tid].insert(make_pair(parentLabel_a, parentLabel_b));
                }
            }
        }
    }// for loop ends
}

void formClusters()
{
		double distance;
		int y, xMin, xMax, maxIndexToSearchAt;
		vector<Node*> listPerYval, listToBeSearched; 
		vector<Node*>::iterator itr, nextListItr;
		for(y = 1; y <= max_Y; y++)
		{
				listPerYval = (*dataPoints[y]);
				if((y + thresholdDistance) < max_Y)
						maxIndexToSearchAt = y + thresholdDistance;
				else
						maxIndexToSearchAt = max_Y;
				for(itr = listPerYval.begin(); itr != listPerYval.end(); ++itr)
				{
						Node *currentPoint = *itr;
						int x = currentPoint->x;
						xMin = ((x - thresholdDistance) < 1) ? 1 : (x - thresholdDistance);
						xMax = ((x + thresholdDistance) > max_X) ? max_X : (x + thresholdDistance);
						for(int i = 0; i < numOfThreads; i++)
						{
								labelPairsPerThread[i].clear(); // TODO:this line is throwing seg fault.. check
						}
#pragma omp parallel for num_threads(numOfThreads) private(listToBeSearched)
						for(int i = y; i <= maxIndexToSearchAt; i++) // Go over only the lists from y to y+threshold
						{
								listToBeSearched = (*dataPoints[i]);
								mergeWithAllNbrs(listToBeSearched, currentPoint, x, y, xMin, xMax);
						}
						for(int i = 0; i < labelPairsPerThread.size(); i++)
						{
								set<pair<Label*,Label*>>::iterator itr;
								for(itr = labelPairsPerThread[i].begin(); itr != labelPairsPerThread[i].end(); ++itr)
								{
										C.mergeClusters(itr->first, itr->second);
								}
						}	
				}	
		}
}


void writeOutput()
{
	const int dir1 = system("mkdir -p results");
	if(dir1 < 0)
	{
		cout << "results directory could not be created.. Exiting.." << endl;
		exit(0);
	}
	string fname;
	fname.append("results/").append(fileName);
	outFile.open(fname, ios::trunc | ios::out);

	outFile  << numOfPoints << "\t" << max_X << "\t" << max_Y << endl; // first line
	Node* currentPoint;
	set<int> uniqueLabels;
	int label;
	vector<Node*> listPerYval;
	vector<Node*>::iterator itr;

	for(int i = 1; i <= max_Y; i++)
	{
		listPerYval = (*dataPoints[i]);
		for(itr = listPerYval.begin(); itr != listPerYval.end(); ++itr)
		{
			currentPoint = *itr;
			label = (C.findRootOfCluster(currentPoint->ptrToLabel))->labelVal;
			outFile << currentPoint->x << "\t" << currentPoint->y << "\t" << label << endl;
			uniqueLabels.insert(label);
		}
	}
	finalNumOfClusters = uniqueLabels.size();
	outFile.close();
}

bool compareNodesOnX(const Node* n1, const Node* n2)
{
	return n1->x < n2->x;
}

int main(int argc, char* argv[])
{
	if(argc != 4)
	{
		cout << "Usage: " << argv[0] << " <INPUT_FILE_NAME> <THRESHOLD_DISTANCE> <NUM_THREADS>" << endl;
		exit(0);
	}
	totalExecTime = 0.0;
	e = std::chrono::steady_clock::now();
	fileName = argv[1];
	thresholdDistance = atoi(argv[2]);
	numOfThreads = atoi(argv[3]);
	labelPairsPerThread.reserve(numOfThreads);
	labelPairsPerThread.resize(numOfThreads);
	ROOT_DIR = getenv("CLUSTERING_ROOT");	
	string inFilename;
	inFilename.append(ROOT_DIR).append("/static/inputsForStaticClustering/").append(fileName);
	int x, y, n;
	Node* currentPoint;
	fstream inputFile(inFilename, ios_base::in);
	/* Read the coordinates of the input points  */
	inputReadTime = 0.0;
	writeOutTime = 0.0;
	c = std::chrono::steady_clock::now();
	inputFile >> n; // Reading the number of points in this input file into variable n.
	inputFile >> max_X;
	inputFile >> max_Y;
	dataPoints.reserve(max_Y + 1); // +1 so that indexing from 1 can be used 
	//	cout << "value of n = " << n << endl;
	// remember to deallocate these lists
	for(int i = 1; i <= max_Y; i++)
	{
		dataPoints[i] = new vector<Node*>;
	}
	// NOTE : For static clustering input, we expect that all the lines except the first one will have only x,y data. 
	while(true)
	{
		if(!(inputFile >> x))
			break;
		inputFile >> y;
		numOfPoints++;
		currentPoint = C.makeSingleElemCluster(x, y, numOfPoints);
		(*dataPoints[y]).push_back(currentPoint);
	}
	d = std::chrono::steady_clock::now();
	inputReadTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(d - c).count()) / 1000.0; // in milli seconds
	// Sorting the lists in the vector based on the x values
	for(int i = 1; i <= max_Y; i++ )
	{
		sort((*dataPoints[i]).begin(), (*dataPoints[i]).end() , compareNodesOnX);
	}

//	cout << "*********************************************" << endl;
//	cout << "num of points = " << numOfPoints << endl;
	clusteringTime = 0.0;
	a = std::chrono::steady_clock::now();
	formClusters();
	b = std::chrono::steady_clock::now();
	clusteringTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(b - a).count()) / 1000.0; // in milli seconds

//	outFile << "Clustering time = " << clusteringTime << " ms = " << clusteringTime / 1000.0 << " seconds" << endl;
	c = std::chrono::steady_clock::now();
	writeOutput();
	d = std::chrono::steady_clock::now();
	writeOutTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(d - c).count()) / 1000.0; // in milli seconds

	for(int i = 1; i <= max_Y; i++)
	{
		delete dataPoints[i];
	}

	f = std::chrono::steady_clock::now();
	totalExecTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(f - e).count()) / 1000.0; // in milli seconds
	cout << numOfPoints << "\t" << clusteringTime / 1000.0 << "\t" << finalNumOfClusters << "\t" << numOfThreads << "\t" << totalExecTime << endl;
	return 0;
}
