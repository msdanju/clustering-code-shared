#include <iostream>
#include <unordered_map>
#include "BasicClusters.h"
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <chrono>
#include <set>
#include <vector>
#include <algorithm>
#include <omp.h>

using namespace std;

BasicClusters C;
int numOfPoints = 0, finalNumOfClusters, n = 0;
int LARGE= 33554432; // =2^25i
ofstream outFile, allResultsFile;
std::chrono::steady_clock::time_point a, b, c, d, e, f, deleteStart, deleteEnd;
float dynClusTime , totalExecTime, inputReadTime, writeOutTime, deletionTime;
string inFileName, fileName;
int thresholdDistance;
vector<vector<Node*>*> dataPoints; // vector[i] contains a pointer to a vector of pointers to Nodes for all points of the form (x,i)
int UPPER_BOUND;
int max_X, max_Y;
int numOfThreads;
vector<set<pair<Label*, Label*>>> labelPairsPerThread; // vector of sets
string dynDataFileName;
set<int> uniqueLabels;
fstream dynInputFile;
int initialNumOfClusters;
string ROOT_DIR;
string dynFileNameWithoutPath;

double findDistance(int x1, int y1, int x2, int y2)
{
		return sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
}


void writeInputForStaticClus()
{
		string finalFileName;
		finalFileName.append(ROOT_DIR).append("/static/inputsForStaticClustering/").append("newData_").append(dynFileNameWithoutPath);
		ofstream f1;
		f1.open(finalFileName, ios::trunc | ios::out);	
		f1 << numOfPoints << "\t" << max_X << "\t" << max_Y << endl; // first line of the file
		vector<Node*> listPerYval;
		vector<Node*>::iterator itr;
		Node* currentPoint;
		for(int i = 1; i <= max_Y; i++)
		{
				listPerYval = (*dataPoints[i]);
				for(itr = listPerYval.begin(); itr != listPerYval.end(); ++itr)
				{
						currentPoint = *itr;
						if(!(currentPoint->isDeleted)) // if not a deleted point
						  f1 << currentPoint->x << "\t" << currentPoint->y  << endl;
				}
		}	
		f1.close();
}

void writeOutput()
{
		const int dir1 = system("mkdir -p results");
		if(dir1 < 0)
		{
				cout << "results directory could not be created.. Exiting.." << endl;
				exit(0);
		}
		string fname;
    fname.append("results/").append(fileName);
    outFile.open(fname, ios::trunc | ios::out);
		outFile << numOfPoints << "\t" << max_X << "\t" << max_Y << endl; // first line of the file
		Node* currentPoint;
		set<int> finalUniqueLabels; // Local variable
		int label;
		vector<Node*> listPerYval;
		vector<Node*>::iterator itr;

		for(int i = 1; i <= max_Y; i++)
		{
				listPerYval = (*dataPoints[i]);
				for(itr = listPerYval.begin(); itr != listPerYval.end(); ++itr)
				{
						currentPoint = *itr;
						if(!(currentPoint->isDeleted)) // if not a deleted point
						{
								label = (C.findRootOfCluster(currentPoint->ptrToLabel))->labelVal;
								outFile << currentPoint->x << "\t" << currentPoint->y << "\t" << label << endl;
								finalUniqueLabels.insert(label);
						}
				}
		}
		finalNumOfClusters = finalUniqueLabels.size();
		outFile.close();
}

bool compareNodesOnX(const Node* n1, const Node* n2)
{
		return n1->x < n2->x;
}



void mergeWithAllNbrs(vector<Node*> &listToBeSearched, Node* currentPoint, int x, int y, int xStart, int xEnd)
{
		//cout << "Entered mergeWithAllNbrs..................." << endl;
		vector<Node*>::iterator nextListItr;
		Node* nextPoint;
		double distance;
		Label *parentLabel_a, *parentLabel_b;
		int tid;
#ifdef _OPENMP
    tid = omp_get_thread_num();
#else
    tid = 0;
#endif	
		for(nextListItr = listToBeSearched.begin(); nextListItr != listToBeSearched.end(); ++nextListItr)
		{
				nextPoint = *nextListItr;
				if(nextPoint->isDeleted)
					continue; // Skip as this neighbour is marked as deleted
				if(nextPoint->x < xStart)
						continue;
				if(nextPoint->x > xEnd)
						break;
				else
				{
		//				cout << nextPoint->x << "\t" << nextPoint->y << endl;
						distance = findDistance(x, y, nextPoint->x, nextPoint->y);
						if(distance <= thresholdDistance)
						{
								parentLabel_a = C.findRootOfCluster(currentPoint->ptrToLabel);
								parentLabel_b = C.findRootOfCluster(nextPoint->ptrToLabel);
								if(parentLabel_a != parentLabel_b)
								{
										labelPairsPerThread[tid].insert(make_pair(parentLabel_a, parentLabel_b));
								}
						}
				}
		}// for loop ends
}

void initGlobalVars()
{
		totalExecTime = 0.0;
		inputReadTime = 0.0;
		writeOutTime = 0.0;
		dynClusTime = 0.0;
		deletionTime = 0.0;


}

void extractCommandLineArgs(char* argv[])
{
		fileName = argv[1];
		thresholdDistance = atoi(argv[2]);
		dynFileNameWithoutPath = argv[3];
		dynDataFileName.append(ROOT_DIR).append("/inputs/").append(argv[3]);
		numOfThreads = atoi(argv[4]);

}

void allocateSpace()
{
// remember to deallocate these vectors
		for(int i = 1; i <= max_Y; i++)
		{
				dataPoints[i] = new vector<Node*>;
		}
		labelPairsPerThread.reserve(numOfThreads);
		labelPairsPerThread.resize(numOfThreads);
}

void readInput()
{
		string inFilename;
		inFilename.append(ROOT_DIR).append("/inputs/").append(fileName);
		int x, y, label;
		Node* currentPoint;
		fstream inputFile;
		inputFile.open(inFilename, ios_base::in);
		if(!(inputFile.is_open()))
		{
				cout << "Could not open the file " << inFilename << ".. Exiting program..." << endl;
				exit(0);
		}
		/* Read the coordinates of the input points  */
		inputFile >> n; // Reading the number of points in this input file into global variable n.
		inputFile >> max_X;
		inputFile >> max_Y;
		// The points will have max values of X and Y coordinates max_X and max_Y respectively. So the vector size is reserved accordingly. 
		dataPoints.reserve(max_Y + 1); /* +1 so that indexing from 1 can be used */
		set<pair<int, int>> allPoints;
		allocateSpace();
		while(true)
		{
				if(!(inputFile >> x))
						break;
				inputFile >> y;
				inputFile >> label;
				numOfPoints++;
				currentPoint = C.makeSingleElemCluster(x, y, label);
				(*dataPoints[y]).push_back(currentPoint);
				allPoints.insert(make_pair(x, y));
				uniqueLabels.insert(label);
		}
		// The vectors are already in sorted order. - ASSUMPTION
		inputFile.close();
		initialNumOfClusters = uniqueLabels.size();
}

void giveNewLabels(vector<Node*> listToBeRelabelled, int xStart, int xEnd)
{
	vector<Node*>::iterator nextListItr;
	Node* nextPoint;
	for(nextListItr = listToBeRelabelled.begin(); nextListItr != listToBeRelabelled.end(); ++nextListItr)
	{
		nextPoint = *nextListItr;
		if(nextPoint->x < xStart)
			continue;
		else if(nextPoint->x > xEnd)
			break;
		//	if(nextPoint->x >= xStart && nextPoint->x <= xEnd)	// This if condition is not needed
		//	{
		int newLabelVal = (*(uniqueLabels.rbegin())) + 1; // Value of the last element in sorted order + 1
		// Creating a new label
		Label *newLabelPtr = new Label();
		if(newLabelPtr == NULL)
		{
			cout << "In giveNewLabels fn.. new operator returned NULL.. Exiting.." << endl;
			exit(0);
		}
		newLabelPtr->labelVal = newLabelVal;
		newLabelPtr->rank = 0;
		newLabelPtr->ptrToParentLabel = newLabelPtr; // make the label its own parent
		pair<set<Label*>::iterator, bool> ret;
		ret = C.allLabels.insert(newLabelPtr); // NOTE- THIS MIGHT CAUSE ISSUE FOR PARALLELISM
		// Hopefully, this insert should never fail, as the label value is new. Check anyways..
		if(ret.second == false) // safety check
		{
			cout << "Insert failed... Take a look... " << endl;	
			exit(0);
		}
		else
				uniqueLabels.insert(newLabelVal); 
		nextPoint->ptrToLabel = newLabelPtr; // Changing the point's label to the new one	
	}
	//	}
}

void reAddPointsWithinThreshold(int xStart, int xEnd, int yStart, int yEnd)
{
/*	for(int i = 0; i < numOfThreads; i++)
  {
  	labelPairsPerThread[i].clear();
  } */
//#pragma omp parallel for num_threads(numOfThreads)
	for(int y = yStart; y <= yEnd; y++)
	{
		vector<Node*> currentRow = (*dataPoints[y]);
		vector<Node*>::iterator itr;
		for(itr = currentRow.begin(); itr != currentRow.end(); ++itr)
		{
			Node *currentPoint = *itr;
			if(currentPoint->isDeleted)
				continue; // This point is marked as deleted, so dont process.
			int x = currentPoint->x;
			if(x < xStart)
				continue;
			else if(x > xEnd)
				break;
		//	cout << currentPoint->x << "\t" << currentPoint->y << endl;
			int y_1 = ((y - thresholdDistance) < 1) ? 1 : (y - thresholdDistance);
			int y_2 = ((y + thresholdDistance) > max_Y ) ? max_Y : (y + thresholdDistance);
			int x_1 = ((x - thresholdDistance) < 1) ? 1 : (x - thresholdDistance);
			int x_2 = ((x + thresholdDistance) > max_X ) ? max_X : (x + thresholdDistance);

			for(int k = 0; k < numOfThreads; k++) // TODO: Keep this loop in the beginning of the function and see.
				labelPairsPerThread[k].clear();

#pragma omp parallel for num_threads(numOfThreads)
			for(int j = y_1; j <= y_2; j++)
			{
				vector<Node*> listToBeSearched = (*dataPoints[j]);
				mergeWithAllNbrs(listToBeSearched, currentPoint, x, y, x_1, x_2);
			}
			for(int i = 0; i < numOfThreads; i++)
			{
					set<pair<Label*,Label*>>::iterator labelPairsItr;
					for(labelPairsItr = labelPairsPerThread[i].begin(); labelPairsItr != labelPairsPerThread[i].end(); ++labelPairsItr)
							C.mergeClusters(labelPairsItr->first, labelPairsItr->second);
			}
			
		}
	} // for loop from yStart to yEnd ends here
	// Sequential merging of labels
/*	for(int i = 0; i < numOfThreads; i++)
	{
			set<pair<Label*,Label*>>::iterator labelPairsItr;
			for(labelPairsItr = labelPairsPerThread[i].begin(); labelPairsItr != labelPairsPerThread[i].end(); ++labelPairsItr)
					C.mergeClusters(labelPairsItr->first, labelPairsItr->second);
	} */
}

void deletePoint(int x, int y)
{
	// Get pointer to the node for (x,y) and call C.deleteNode()
	vector<Node*>* currentList = (dataPoints[y]);
	Node *currentPoint = C.makeSingleElemCluster(x, y, -1); // make a dummy node
	vector<Node*>::iterator deletePosition = lower_bound(currentList->begin(), currentList->end(), currentPoint, compareNodesOnX);

	vector<Node*>::iterator itr;
	Node *elem;
	for(itr = currentList->begin(); itr != currentList->end(); ++itr)
	{
			elem = *itr;
			if(elem->x == x && elem->y == y)
					break;
	}

	if(itr == currentList->end())
	{
			cout << "Element (" << x << "," << y << ") not found in the DS" << endl;
			exit(0);
	}
	Node* elemToBeDeleted = *itr;
	if(elemToBeDeleted->x != x)
	{
			cout << "Element given for deletion (" << x << "," << y << ") not found in the DS" << endl;
			exit(0);
	}
	if(elemToBeDeleted->isDeleted)
	{
		cout << "(" << x << "," << y << ") is already marked as deleted" << endl;
		exit(0);
	}
	C.deleteNode(elemToBeDeleted); // TODO: check isDeleted at the time of distance check while adding new points
	// Get yStart, yEnd, xStart, xEnd; and for all those nodes, give new label values and make the label's parent pointer point to itself
	int xStart, xEnd, yStart, yEnd;
	yStart = ((y - thresholdDistance) < 1) ? 1 : (y - thresholdDistance);
	yEnd = ((y + thresholdDistance) > max_Y ) ? max_Y : (y + thresholdDistance);
	xStart = ((x - thresholdDistance) < 1) ? 1 : (x - thresholdDistance);
	xEnd = ((x + thresholdDistance) > max_X ) ? max_X : (x + thresholdDistance);

	for(int i = yStart; i <= yEnd; i++)
	{
		vector<Node*> listToBeRelabelled = (*dataPoints[i]);
		giveNewLabels(listToBeRelabelled, xStart, xEnd);
	}

	// For all these points, do distance computations and merge with labels
	reAddPointsWithinThreshold(xStart, xEnd, yStart, yEnd);
	 
}

void doDynamicClustering()
{
		dynInputFile.open(dynDataFileName, ios_base::in);
		if(!(dynInputFile.is_open()))
		{
				cout << "Could not open the file " << dynDataFileName << "..Exiting program.." << endl;
				exit(0);
		}
		int x,y;
		int addOrDel; // 1 => add, 0 => del
		while(true)
		{
				if(!(dynInputFile >> x))
						break;
				dynInputFile >> y;
				dynInputFile >> addOrDel;
				deleteStart = std::chrono::steady_clock::now();
				if(addOrDel == 0) // Delete point
				{
						deletePoint(x, y);
						numOfPoints--; // TODO- consider cases like the point was already deleted, the point doesnt exist, etc
				}
				deleteEnd = std::chrono::steady_clock::now();
				deletionTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(deleteEnd - deleteStart).count()) / 1000.0; // in milli seconds
				//TODO - Remove later
				if(deletionTime / 1000 > 300 ) // if deltime is > 5 hrs, then skip
						break;
		}
		// TODO - edit this line. dynClusTime = insertToDStime + mergeTime; // This line is executed only once
		dynInputFile.close();

}



void deallocateSpace()
{
		for(int i = 1; i <= max_Y; i++)
		{
				delete dataPoints[i];
		}
}

int main(int argc, char* argv[])
{
		if(argc != 5)
		{
				cout << "Usage: " << argv[0] << " <INPUT_FILE_NAME> <THRESHOLD_DISTANCE> <NAME_OF_FILE_WITH_DYN_DATA> <NUM_THREADS>" << endl;
				exit(0);
		}
		e = std::chrono::steady_clock::now();
		ROOT_DIR = getenv("CLUSTERING_ROOT");
		extractCommandLineArgs(argv);
		initGlobalVars();
		readInput();
		doDynamicClustering();
		writeOutput();					
		writeInputForStaticClus();
		deallocateSpace();
		
		f = std::chrono::steady_clock::now();
		totalExecTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(f - e).count()) / 1000.0; // in milli seconds
		cout << numOfPoints << "\t" << deletionTime / 1000.0 << "\t" << finalNumOfClusters <<  "\t" << numOfThreads << "\t" << initialNumOfClusters  << "\t" << fileName << "\t" << argv[3] <<  endl;
		return 0;
}
