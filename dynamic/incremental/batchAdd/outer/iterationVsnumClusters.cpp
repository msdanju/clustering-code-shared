#include <iostream>
#include <unordered_map>
#include "BasicClusters.h"
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <chrono>
#include <set>
#include <vector>
#include <algorithm>
#include <map>
#include <iomanip>
#include <omp.h>
using namespace std;

BasicClusters C;
int numOfPoints = 0, finalNumOfClusters, n = 0;
int LARGE= 33554432; // =2^25i
ofstream outFile, allResultsFile;
std::chrono::steady_clock::time_point a, b, c, d, e, f, mergeStart, mergeEnd, insertStart, insertEnd, readAndPreprocessStart, readAndPreprocessEnd, actualInsertionInMapStart, actualInsertionInMapEnd, readXYStart, readXYEnd, singleElClusStart, singleElClusEnd, newPointsMapStart, newPointsMapEnd, findLabelsForMergingStart, findLabelsForMergingEnd, actualMergeStart, actualMergeEnd, findDistanceStart, findDistanceEnd, findRootsStart, findRootsEnd, addToSetStart, addToSetEnd, forLoopStart, forLoopEnd;
float dynClusTime , totalExecTime, inputReadTime, writeOutTime, insertToDStime, mergeTime, readAndPreprocessTime, actualInsertionInMapTime, actualMergeTime;
string inFileName, fileName;
int thresholdDistance;
vector<vector<Node*>*> dataPoints; // vector[i] contains a pointer to a vector of pointers to Nodes for all points of the form (x,i)
int UPPER_BOUND;
int max_X, max_Y;
int numOfThreads;
map<int, vector<Node*>> newPoints;
int batchSize;
vector<int> newYvals;
float readXYTime, singleElClusTime, newPointsMapTime, sumOf3, findPairsTime;
set<int> uniqueLabels;
string dynDataFileName;
bool moreDataToRead, someDataRead;
fstream dynInputFile;
vector<set<pair<Label*, Label*>>> labelPairsPerThread; // vector of sets
float findDistanceTime, findRootsTime, addToSetTime, forLoopTime;
string ROOT_DIR;

double findDistance(int x1, int y1, int x2, int y2)
{
		return sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
}

int countClusters()
{
    Node* currentPoint;
    set<int> uniqueClusterLabels; // Local variable
    int label;
    vector<Node*> listPerYval;
    vector<Node*>::iterator itr;

    for(int i = 1; i <= max_Y; i++)
    {
        listPerYval = (*dataPoints[i]);
        for(itr = listPerYval.begin(); itr != listPerYval.end(); ++itr)
        {
            currentPoint = *itr;
            if(!(currentPoint->isDeleted)) // if not a deleted point
            {
                label = (C.findRootOfCluster(currentPoint->ptrToLabel))->labelVal;
                uniqueClusterLabels.insert(label);
            }
        }
    }
    return uniqueClusterLabels.size();
}

void writeInputForStaticClus()
{
		string finalFileName;
		//TODO: Remove the hard-coded path  
		finalFileName.append(ROOT_DIR).append("/static/inputsForStaticClustering/").append("newData_").append(fileName);
		ofstream f1;
		f1.open(finalFileName, ios::trunc | ios::out);	
		f1 << numOfPoints << "\t" << max_X << "\t" << max_Y << endl; // first line of the file
		vector<Node*> listPerYval;
		vector<Node*>::iterator itr;
		Node* currentPoint;
		for(int i = 1; i <= max_Y; i++)
		{
				listPerYval = (*dataPoints[i]);
				for(itr = listPerYval.begin(); itr != listPerYval.end(); ++itr)
				{
						currentPoint = *itr;
						f1 << currentPoint->x << "\t" << currentPoint->y  << endl;
				}
		}	
		f1.close();
}

void writeOutput()
{
		const int dir1 = system("mkdir -p results");
		if(dir1 < 0)
		{
				cout << "results directory could not be created.. Exiting.." << endl;
				exit(0);
		}
		string fname;
    fname.append("results/").append(fileName);
    outFile.open(fname, ios::trunc | ios::out);
		outFile << numOfPoints << "\t" << max_X << "\t" << max_Y << endl; // first line of the file
		Node* currentPoint;
		uniqueLabels.clear();
		int label;
		vector<Node*> listPerYval;
		vector<Node*>::iterator itr;

		for(int i = 1; i <= max_Y; i++)
		{
				listPerYval = (*dataPoints[i]);
				for(itr = listPerYval.begin(); itr != listPerYval.end(); ++itr)
				{
						currentPoint = *itr;
						label = (C.findRootOfCluster(currentPoint->ptrToLabel))->labelVal;
						outFile << currentPoint->x << "\t" << currentPoint->y << "\t" << label << endl;
						uniqueLabels.insert(label);
				}
		}
		finalNumOfClusters = uniqueLabels.size();
		outFile.close();
}

bool compareNodesOnX(const Node* n1, const Node* n2)
{
		return n1->x < n2->x;
}

/*void processLeftMostNbrWithinThreshold(vector<Node*> &listToBeSearched, int x, int y, int xStart, int xEnd, Node *currentPoint)
	{
	vector<Node*>::iterator nextListItr;
	Node* nextPoint;
	double distance;
	for(nextListItr = listToBeSearched.begin(); nextListItr != listToBeSearched.end(); ++nextListItr)
	{
	nextPoint = *nextListItr;
	if(nextPoint->x < xStart)
	continue;
	if(nextPoint->x > x) //End) // Checking only from the left till the middle point 
	break;
	else
	{
	distance = findDistance(x, y, nextPoint->x, nextPoint->y);
	if(distance <= thresholdDistance)
	{
#pragma omp critical(mergeCS)	
{
C.mergeClusters(currentPoint, nextPoint); // Only this line is part of the CS
}
break; // Left most point on the same y line within the threshold is found and the label is merged.
}
}
} // for loop on listToBeSearched ends
}

void processRightMostNbrWithinThreshold(vector<Node*> listToBeSearched, int x, int y, int xEnd, Node *currentPoint)
{
vector<Node*>::reverse_iterator revItr;
Node* nextPoint;
double distance;
for(revItr = listToBeSearched.rbegin(); revItr != listToBeSearched.rend(); ++revItr)
{
nextPoint = *revItr;
if(nextPoint->x > xEnd)
continue;
else if(nextPoint->x <= x)
break; // middle point is crossed. No need to check further on the left
else
{
distance = findDistance(x, y, nextPoint->x, nextPoint->y);
if(distance <= thresholdDistance)
{
#pragma omp critical(mergeCS)	
C.mergeClusters(currentPoint, nextPoint); // Only this line is in the CS
break; // Left most point on the same y line within the threshold is found and the label is merged.
}
}
}// For loop using reverse iterator ends
}
*/
/* This function merges the label of currentPoint with the labels of all the nbrs in a particular y line(listToBeSearched) */
void mergeWithAllNbrs(vector<Node*> &listToBeSearched, Node* currentPoint, int x, int y, int xStart, int xEnd)
{
		vector<Node*>::iterator nextListItr;
		Node* nextPoint;
		double distance;
		Label *parentLabel_a, *parentLabel_b;
		int tid;
#ifdef _OPENMP
		tid = omp_get_thread_num();
#else
		tid = 0;
#endif

	//	forLoopStart = std::chrono::steady_clock::now();
		for(nextListItr = listToBeSearched.begin(); nextListItr != listToBeSearched.end(); ++nextListItr)
		{
				nextPoint = *nextListItr;
				if(nextPoint->x < xStart)
						continue;
				if(nextPoint->x > xEnd)
						break;
				else
				{
		//				findDistanceStart = std::chrono::steady_clock::now();
						distance = findDistance(x, y, nextPoint->x, nextPoint->y);
		//				findDistanceEnd = std::chrono::steady_clock::now();
		//				findDistanceTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(findDistanceEnd - findDistanceStart).count()) / 1000.0; // in milli seconds

						if(distance <= thresholdDistance)
						{

			//					findRootsStart = std::chrono::steady_clock::now();
								parentLabel_a = C.findRootOfCluster(currentPoint->ptrToLabel);
								parentLabel_b = C.findRootOfCluster(nextPoint->ptrToLabel);
			//					findRootsEnd = std::chrono::steady_clock::now();
			//					findRootsTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(findRootsEnd - findRootsStart).count()) / 1000.0; // in milli seconds

								if(parentLabel_a != parentLabel_b)
								{

				//						addToSetStart = std::chrono::steady_clock::now();
										labelPairsPerThread[tid].insert(make_pair(parentLabel_a, parentLabel_b));
				//						addToSetEnd = std::chrono::steady_clock::now();
				//						addToSetTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(addToSetEnd - addToSetStart).count()) / 1000.0; // in milli seconds
										//											C.mergeClusters(parentLabel_a, parentLabel_b);
								}
						}
				}
		}// for loop ends
		//forLoopEnd = std::chrono::steady_clock::now();
		//forLoopTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(forLoopEnd - forLoopStart).count()) / 1000.0; // in milli seconds
}

/*void mergeWithOnlyLMAndRMNbrs(vector<Node*> &listToBeSearched, Node* currentPoint, int x, int y, int xStart, int xEnd)
	{
	vector<Node*>::iterator nextListItr;
	Node* nextPoint;
	double distance;
	bool isLMNbrFound = false;
	bool isRMNbrFound = false;
	Label *parentLabel_a, *parentLabel_b;
	for(nextListItr = listToBeSearched.begin(); nextListItr != listToBeSearched.end(); ++nextListItr)
	{
	nextPoint = *nextListItr;
	if(nextPoint->x < xStart)
	continue;
	if(nextPoint->x > xEnd || (isLMNbrFound && isRMNbrFound))
	break;
	else if(isLMNbrFound == false ||  nextPoint->x > x)
	{
	distance = findDistance(x, y, nextPoint->x, nextPoint->y);
	if(distance <= thresholdDistance)
	{
	parentLabel_a = C.findRootOfCluster(currentPoint->ptrToLabel);
	parentLabel_b = C.findRootOfCluster(nextPoint->ptrToLabel);
	if(parentLabel_a != parentLabel_b)
#pragma omp critical(mergeCS)
{
C.mergeClusters(parentLabel_a, parentLabel_b);
}
if(nextPoint->x <= x)
isLMNbrFound = true;
else
isRMNbrFound = true;
}
}
}
}*/



void mergeLabelsOfNewlyAddedPoints()
{
		for(int i = 0; i < numOfThreads; i++)
						{
								labelPairsPerThread[i].clear();
						}
#pragma omp parallel for num_threads(numOfThreads)
		for(int i=0; i < newYvals.size(); i++)
		{
				// Find out if there is a neighbour within threshold distance and if yes, merge the labels
				int x, y, xStart, xEnd, yStart, yEnd;
				y = newYvals[i]; // Possibilities of false sharing on this array??
				vector<Node*> newList = newPoints[y]; // Get the new points with the specific y value from the map
				for(vector<Node*>::iterator itr = newList.begin(); itr != newList.end(); ++itr)
				{ 
						Node *currentPoint = *itr;
						x = currentPoint->x;
						yStart = ((y - thresholdDistance) < 1) ? 1 : (y - thresholdDistance);		
						yEnd = ((y + thresholdDistance) > max_Y ) ? max_Y : (y + thresholdDistance);
						xStart = ((x - thresholdDistance) < 1) ? 1 : (x - thresholdDistance);
						xEnd = ((x + thresholdDistance) > max_X ) ? max_X : (x + thresholdDistance);
						vector<Node*> listToBeSearched;
//#pragma omp parallel for num_threads(numOfThreads) private(listToBeSearched)
						for(int j = yStart; j <= yEnd; j++) // Go over each list in the vector dataPoints at index i.
						{
								listToBeSearched = (*dataPoints[j]);
								mergeWithAllNbrs(listToBeSearched, currentPoint, x, y, xStart, xEnd);
						}
				} // for loop over newList for a particular y ends
		} // for loop over all the new y-lists

		for(int i = 0; i < numOfThreads; i++)
						{
								set<pair<Label*,Label*>>::iterator itr;
								for(itr = labelPairsPerThread[i].begin(); itr != labelPairsPerThread[i].end(); ++itr)
								{
										C.mergeClusters(itr->first, itr->second);
								}
						}

}


/* This function inserts the complete batch of points for addition to the data structure dataPoints. */
void insertToDS()
{
		//cout << "Entered insertToDS.. " << endl; 
		int y;
#pragma omp parallel for num_threads(numOfThreads) private(y)
		for(int i=0; i < newYvals.size(); i++)
		{
				y = newYvals[i];
				// Use this y val as the key and get the list of points to be added for each such y. 
				vector<Node*>* currentList = (dataPoints[y]);
				vector<Node*> newList = newPoints[y]; // Get the new points with the specific y value from the map
				for(vector<Node*>::iterator itr = newList.begin(); itr != newList.end(); ++itr)
				{ 
						Node *currentPoint = *itr;
						vector<Node*>::iterator insertPosition = lower_bound(currentList->begin(), currentList->end(), currentPoint, compareNodesOnX);	
						// Iterator now points to the element which is greater than or equal to the currentPoint, based on the comparator.
						// Since the dynamic data can have duplicate points, check if the iterator points to an element with the same X value, and if yes, skip further processing - TO-DO
						if(currentList->size() > 0 && insertPosition != currentList->end()) 
						{
								Node* elemAtInsertPosition = *insertPosition;
								if( elemAtInsertPosition->x == currentPoint->x)
								{
										cout << "Trying to add an existing point - (" << currentPoint->x << ", " << currentPoint->y << ")...Skipping the processing..." << endl;
										numOfPoints--;
										//return;
										continue;
								}
						} 
						currentList->insert(insertPosition, currentPoint); // Inserts before the element pointed to by the iterator
				}
		}
}

void initGlobalVars()
{
		totalExecTime = 0.0;
		inputReadTime = 0.0;
		writeOutTime = 0.0;
		dynClusTime = 0.0;
		readXYTime = 0.0;
		singleElClusTime = 0.0;
		newPointsMapTime = 0.0;
		insertToDStime = 0.0;
		mergeTime = 0.0;
		readAndPreprocessTime = 0.0;
		sumOf3 = 0.0;
		findPairsTime = 0.0;
		actualMergeTime = 0.0;
		findDistanceTime = 0.0;
		findRootsTime = 0.0;
		addToSetTime = 0.0;
		forLoopTime = 0.0;
}

void allocateSpace()
{
		// remember to deallocate these vectors
		for(int i = 1; i <= max_Y; i++)
		{
				dataPoints[i] = new vector<Node*>;
		}
		labelPairsPerThread.reserve(numOfThreads);
		labelPairsPerThread.resize(numOfThreads);
		//		labelPairsPerThread = (set<pair<Label*, Label*>>**)malloc(numOfThreads * sizeof(set<pair<Label*, Label*>>*));
		/*cout << "alloc entering" << endl;
			for(int i = 0; i < numOfThreads; i++)
			{
			labelPairsPerThread[i] = new set<pair<Label*, Label*>>;	
			}
			cout << "alloc done "<< endl;*/
}

void readInput()
{
		string inFilename;
		inFilename.append(ROOT_DIR).append("/inputs/").append(fileName);
		int x, y, label;
		Node* currentPoint;
		fstream inputFile;
		inputFile.open(inFilename, ios_base::in);
		if(!(inputFile.is_open()))
		{
				cout << "Could not open the file " << inFilename << ".. Exiting program..." << endl;
				exit(0);
		}
		/* Read the coordinates of the input points  */
		c = std::chrono::steady_clock::now();
		inputFile >> n; // Reading the number of points in this input file into global variable n.
		inputFile >> max_X;
		inputFile >> max_Y;
		// n points are there in the input file. And we are going to add 1000 points. But these 1000 points will have max values of X and Y coordinates max_X and max_Y respectively. So the vector size is reserved accordingly. 
		dataPoints.reserve(max_Y + 1); /* +1 so that indexing from 1 can be used */
		set<pair<int, int>> allPoints;
		allocateSpace();
		while(true)
		{
				if(!(inputFile >> x))
						break;
				inputFile >> y;
				inputFile >> label;
				numOfPoints++;
				currentPoint = C.makeSingleElemCluster(x, y, label);
				(*dataPoints[y]).push_back(currentPoint);
				allPoints.insert(make_pair(x, y));
				uniqueLabels.insert(label);
		}
		inputFile.close();// The vectors are already in sorted order. - ASSUMPTION
		d = std::chrono::steady_clock::now();
		inputReadTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(d - c).count()) / 1000.0; // in milli seconds
}

void readBatchAndAddToMap()
{
		int initialLabel, addOrDel, x, y;// 1 => add, 0 => del
		Node *currentPoint;
		newPoints.clear(); // clearing the map
		newYvals.clear(); // clearing the vector
		for(int i = 0; i < batchSize; i++)
		{
				if(!(dynInputFile >> x))
				{
						moreDataToRead =  false;
						break;
				}
				dynInputFile >> y;
				dynInputFile >> addOrDel;

				initialLabel = (*(uniqueLabels.rbegin())) + 1; // Value of the last element in sorted order + 1
				uniqueLabels.insert(initialLabel);
				currentPoint = C.makeSingleElemCluster(x, y, initialLabel);

				newPoints[y].push_back(currentPoint);
				if(find(newYvals.begin(), newYvals.end(), y) == newYvals.end()) // if y is not already in the vector, then only push_back
				{
						newYvals.push_back(y);
				}
				numOfPoints++;
				someDataRead = true;
		} // for loop reading points in batches ends
}

void doDynamicClustering()
{
		/* Read the new (not already existing) points from a file and add them to the clusters dynamically one at a time. ie., insert them in the right place in the data structure. Measure the cumulative time for each addition. */	
		dynInputFile.open(dynDataFileName, ios_base::in);
		if(!(dynInputFile.is_open()))
		{
				cout << "Could not open the file " << dynDataFileName << ".. Exiting program..." << endl;
				exit(0);
		}
		int batchNumber = 0;
		cout << batchNumber << "\t" << countClusters() << endl;
		moreDataToRead = true;
		while(true)
		{
				someDataRead = false;
				readBatchAndAddToMap();
				//cout << "batch read done.." << endl;
				if(someDataRead)
				{
						insertStart = std::chrono::steady_clock::now();
						insertToDS();
						insertEnd = std::chrono::steady_clock::now();
						insertToDStime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(insertEnd - insertStart).count()) / 1000.0; // in milli seconds
						//cout << "insertToDS over.." << endl;
						mergeStart = std::chrono::steady_clock::now();
						mergeLabelsOfNewlyAddedPoints(); 
						mergeEnd = std::chrono::steady_clock::now();
						mergeTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(mergeEnd - mergeStart).count()) / 1000.0; // in milli seconds
						batchNumber++;
						cout << batchNumber << "\t" << countClusters() << endl;
				}
				if(moreDataToRead == false)
						break;

				//cout << "merge over..." << endl;
		} // while loop reading all the new points ends
		dynClusTime = insertToDStime + mergeTime; // This line is executed only once
		dynInputFile.close();
		//cout << "end of dyn clustering " << endl;
}



void deallocateSpace()
{
		for(int i = 1; i <= max_Y; i++)
		{
				delete dataPoints[i];
		}
		//free(labelPairsPerThread);
		/*for(int i=0; i < numOfThreads; i++)
			{
			delete labelPairsPerThread[i];
			}*/
}

void extractCommandLineArgs(char* argv[])
{
		fileName = argv[1];
		thresholdDistance = atoi(argv[2]);
		dynDataFileName.append(ROOT_DIR).append("/inputs/").append(argv[3]);
		numOfThreads = atoi(argv[4]);
		batchSize = atoi(argv[5]);

}

int main(int argc, char* argv[])
{
		if(argc != 6)
		{
				cout << "Usage: " << argv[0] << " <INPUT_FILE_NAME> <THRESHOLD_DISTANCE> <NAME_OF_FILE_WITH_DYN_DATA> <NUM_THREADS> <ADD_BATCH_SIZE>" << endl;
				exit(0);
		}
		ROOT_DIR = getenv("CLUSTERING_ROOT");
		e = std::chrono::steady_clock::now();
		extractCommandLineArgs(argv);
		initGlobalVars();
		readInput();
		//cout << "Input reading done.. " << endl;
		doDynamicClustering();	
		writeOutput();				
		//writeInputForStaticClus();
		deallocateSpace();

		f = std::chrono::steady_clock::now();
		totalExecTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(f - e).count()) / 1000.0; // in milli seconds

		//		cout << fixed << setprecision(8) << numOfPoints << "\t" << dynClusTime / 1000.0 << "\t\t" << finalNumOfClusters <<  "\t" << numOfThreads << "\t\t" << (insertToDStime / dynClusTime) * 100.0 << "\t\t" <<  (mergeTime / dynClusTime) * 100.0 <<  "\t\t" << batchSize << "\t\t" << (findPairsTime / dynClusTime) * 100.0 << "\t\t" << (actualMergeTime / dynClusTime) * 100.0 <<  endl;

		/*		findDistanceTime = 0.0;
					findRootsTime = 0.0;
					addToSetTime = 0.0; */

		cout << fixed << setprecision(8) << numOfPoints << "\t" << dynClusTime / 1000.0 << "\t\t" << finalNumOfClusters <<  "\t" << numOfThreads << "\t\t" << (insertToDStime / dynClusTime) * 100.0 << "\t\t" <<  (mergeTime / dynClusTime) * 100.0 <<  "\t\t" << batchSize  << "\t" << argv[1] <<  "\t" << totalExecTime / 60000 << endl; // totalExecTime is in minutes
//				<< "\t"  <<  findPairsTime / 1000.0 << "\t" <<  forLoopTime / 1000.0 << "\t" << findDistanceTime / 1000.0 << "\t" <<  findRootsTime / 1000.0 << "\t" << addToSetTime / 1000.0 << "\t" << actualMergeTime / 1000.0 << endl;

		/*cout << fixed << setprecision(12) << "dynClustTime : " << dynClusTime / 1000.0 << endl;
		cout << "insertToDStime : " << insertToDStime / 1000.0 << endl;
		cout << "mergeTime      : " << mergeTime / 1000.0 << endl;
		cout << "findPairsTime  : " << findPairsTime / 1000.0 << endl;*/
		return 0;
}
