#include <iostream>
#include <unordered_map>
#include "BasicClusters.h"
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <chrono>
#include <set>
#include <vector>
#include <algorithm>
#include <omp.h>

using namespace std;

BasicClusters C;
int numOfPoints = 0, finalNumOfClusters, n = 0;
int LARGE= 33554432; // =2^25i
ofstream outFile, allResultsFile;
std::chrono::steady_clock::time_point a, b, c, d, e, f, insertStart, insertEnd, mergeStart, mergeEnd;
float dynClusTime , totalExecTime, inputReadTime, writeOutTime, insertToDStime, mergeTime;
string inFileName, fileName;
int thresholdDistance;
vector<vector<Node*>*> dataPoints; // vector[i] contains a pointer to a vector of pointers to Nodes for all points of the form (x,i)
int UPPER_BOUND;
int max_X, max_Y;
int numOfThreads;
vector<set<pair<Label*, Label*>>> labelPairsPerThread; // vector of sets
vector<set<Label*>> labelsPerThread;
set<Label*> allOtherLabelsToBeMerged;
string ROOT_DIR;
string resultFileName;
vector<vector<Label*>> vectorOfLabelsPerThread;


double findDistance(int x1, int y1, int x2, int y2)
{
		return sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
}


void writeInputForStaticClus()
{
		string finalFileName;
		finalFileName.append(ROOT_DIR).append("/static/inputsForStaticClustering/").append("newData_").append(fileName);
		ofstream f1;
		f1.open(finalFileName, ios::trunc | ios::out);	
		f1 << numOfPoints << "\t" << max_X << "\t" << max_Y << endl; // first line of the file
		vector<Node*> listPerYval;
		vector<Node*>::iterator itr;
		Node* currentPoint;
		for(int i = 1; i <= max_Y; i++)
		{
				listPerYval = (*dataPoints[i]);
				for(itr = listPerYval.begin(); itr != listPerYval.end(); ++itr)
				{
						currentPoint = *itr;
						f1 << currentPoint->x << "\t" << currentPoint->y  << endl;
				}
		}	
		f1.close();
}

void writeOutput()
{
		const int dir1 = system("mkdir -p results");
		if(dir1 < 0)
		{
				cout << "results directory could not be created.. Exiting.." << endl;
				exit(0);
		}
		string fname;
		fname.append("results/").append(resultFileName); // changing so that one result file will be produced per dynamic input file
    //fname.append("results/").append(fileName);
    outFile.open(fname, ios::trunc | ios::out);
		outFile << numOfPoints << "\t" << max_X << "\t" << max_Y << endl; // first line of the file
		Node* currentPoint;
		set<int> uniqueLabels;
		int label;
		vector<Node*> listPerYval;
		vector<Node*>::iterator itr;

		for(int i = 1; i <= max_Y; i++)
		{
				listPerYval = (*dataPoints[i]);
				for(itr = listPerYval.begin(); itr != listPerYval.end(); ++itr)
				{
						currentPoint = *itr;
						label = (C.findRootOfCluster(currentPoint->ptrToLabel))->labelVal;
						outFile << currentPoint->x << "\t" << currentPoint->y << "\t" << label << endl;
						uniqueLabels.insert(label);
				}
		}
		finalNumOfClusters = uniqueLabels.size();
		outFile.close();
}

bool compareNodesOnX(const Node* n1, const Node* n2)
{
		return n1->x < n2->x;
}

void insertToDS(int vectorIndex, Node* currentPoint)
{
		vector<Node*>* currentList = (dataPoints[vectorIndex]);
		vector<Node*>::iterator insertPosition = lower_bound(currentList->begin(), currentList->end(), currentPoint, compareNodesOnX);
		// Iterator now points to the element which is greater than or equal to the currentPoint, based on the comparator.
		// Since the dynamic data can have duplicate points, check if the iterator points to an element with the same X value, and if yes, skip further processing - TO-DO
		if(currentList->size() > 0 && insertPosition != currentList->end()) 
		{
				Node* elemAtInsertPosition = *insertPosition;
				if( elemAtInsertPosition->x == currentPoint->x)
				{
						cout << "Trying to add an existing point - (" << currentPoint->x << ", " << currentPoint->y << ")...Skipping the processing..." << endl;
						numOfPoints--;
						return;
				}
		} 
		currentList->insert(insertPosition, currentPoint); // Inserts before the element pointed to by the iterator
}

/* This function merges the label of currentPoint with the labels of all the nbrs in a particular y line(listToBeSearched) */
/*void mergeWithAllNbrs(vector<Node*> &listToBeSearched, Node* currentPoint, int x, int y, int xStart, int xEnd)
	{
	vector<Node*>::iterator nextListItr;
	Node* nextPoint;
	double distance;

	for(nextListItr = listToBeSearched.begin(); nextListItr != listToBeSearched.end(); ++nextListItr)
	{
	nextPoint = *nextListItr;
	if(nextPoint->x < xStart)
	continue;
	if(nextPoint->x > xEnd)
	break;
	else
	{
	distance = findDistance(x, y, nextPoint->x, nextPoint->y);
	if(distance <= thresholdDistance)
	{
#pragma omp critical(mergeCS)	
{					
C.mergeClusters(currentPoint, nextPoint);
}
}
}
}
} */

void mergeWithAllNbrs(vector<Node*> &listToBeSearched, Node* currentPoint, int x, int y, int xStart, int xEnd, Label* parentLabel_a)
{
		vector<Node*>::iterator nextListItr;
		Node* nextPoint;
		double distance;
		Label *parentLabel_b;
		int tid;
#ifdef _OPENMP
    tid = omp_get_thread_num();
#else
    tid = 0;
#endif	
		for(nextListItr = listToBeSearched.begin(); nextListItr != listToBeSearched.end(); ++nextListItr)
		{
				nextPoint = *nextListItr;
				if(nextPoint->x < xStart)
						continue;
				if(nextPoint->x > xEnd)
						break;
				else
				{
						distance = findDistance(x, y, nextPoint->x, nextPoint->y);
						if(distance <= thresholdDistance)
						{
								//TODO- findRoot call for the newly added point will always return the same new label here, as there is no update of parent pointer at this stage. So, we can as well do it only once. Reducing the amount of memory accesses. Will the compiler be already doing this optimization?
//							parentLabel_a = C.findRootOfCluster(currentPoint->ptrToLabel);
//							labelPairsPerThread[tid].insert(make_pair(parentLabel_a, parentLabel_b));
								if(distance != 0) // distance == 0 implies it is the newLabel itself. So no need to merge
								{
									parentLabel_b = C.findRootOfCluster(nextPoint->ptrToLabel);
									labelsPerThread[tid].insert(parentLabel_b);
								}
						}
				}
		}// for loop ends
}

/*void processLeftMostNbrWithinThreshold(vector<Node*> &listToBeSearched, int x, int y, int xStart, int xEnd, Node *currentPoint)
{
		vector<Node*>::iterator nextListItr;
		Node* nextPoint;
		double distance;
		for(nextListItr = listToBeSearched.begin(); nextListItr != listToBeSearched.end(); ++nextListItr)
		{
				nextPoint = *nextListItr;
				if(nextPoint->x < xStart)
						continue;
				if(nextPoint->x > x) //End) // Checking only from the left till the middle point
						break;
				else
				{
						distance = findDistance(x, y, nextPoint->x, nextPoint->y);
						if(distance <= thresholdDistance)
						{
#pragma omp critical(mergeCS)
								{
										C.mergeClusters(currentPoint, nextPoint); // Only this line is part of the CS
								}
								break; // Left most point on the same y line within the threshold is found and the label is merged.
						}
				}
		} // for loop on listToBeSearched ends
}*/

/*void processRightMostNbrWithinThreshold(vector<Node*> listToBeSearched, int x, int y, int xEnd, Node *currentPoint)
{
		vector<Node*>::reverse_iterator revItr;
		Node* nextPoint;
		double distance;
		for(revItr = listToBeSearched.rbegin(); revItr != listToBeSearched.rend(); ++revItr)
		{
				nextPoint = *revItr;
				if(nextPoint->x > xEnd)
						continue;
				else if(nextPoint->x <= x)
						break; // middle point is crossed. No need to check further on the left
				else
				{
						distance = findDistance(x, y, nextPoint->x, nextPoint->y);
						if(distance <= thresholdDistance)
						{
#pragma omp critical(mergeCS)
								C.mergeClusters(currentPoint, nextPoint); // Only this line is in the CS
								break; // Left most point on the same y line within the threshold is found and the label is merged.
						}
				}
		}// For loop using reverse iterator ends
} */

/*void mergeWithOnlyLMAndRMNbrs(vector<Node*> &listToBeSearched, Node* currentPoint, int x, int y, int xStart, int xEnd)
{
		vector<Node*>::iterator nextListItr;
		Node* nextPoint;
		double distance;
		bool isLMNbrFound = false;
		bool isRMNbrFound = false;
		for(nextListItr = listToBeSearched.begin(); nextListItr != listToBeSearched.end(); ++nextListItr)
		{
				nextPoint = *nextListItr;
				if(nextPoint->x < xStart)
						continue;
				if(nextPoint->x > xEnd || (isLMNbrFound && isRMNbrFound))
						break;
				else if(isLMNbrFound == false ||  nextPoint->x > x)
				{
						distance = findDistance(x, y, nextPoint->x, nextPoint->y);
						if(distance <= thresholdDistance)
						{
#pragma omp critical(mergeCS)
								{
										C.mergeClusters(currentPoint, nextPoint);
								}
								if(nextPoint->x <= x)
										isLMNbrFound = true;
								else
										isRMNbrFound = true;
						}
				}
		}
}*/

void mergeLabelOfNewlyAddedPoint(Node* currentPoint)
{
		// Find out if there is a neighbour within threshold distance and if yes, merge the labels
		int x, y, xStart, xEnd, yStart, yEnd;
		x = currentPoint->x;
		y =  currentPoint->y;
		yStart = ((y - thresholdDistance) < 1) ? 1 : (y - thresholdDistance);		
		yEnd = ((y + thresholdDistance) > max_Y ) ? max_Y : (y + thresholdDistance);
		xStart = ((x - thresholdDistance) < 1) ? 1 : (x - thresholdDistance);
		xEnd = ((x + thresholdDistance) > max_X ) ? max_X : (x + thresholdDistance);
		vector<Node*> listToBeSearched;
//		#pragma omp parallel for num_threads(numOfThreads)
		for(int i = 0; i < numOfThreads; i++)
		{
				//labelPairsPerThread[i].clear();
				labelsPerThread[i].clear();
		} 
		//allOtherLabelsToBeMerged.clear();
		Label *newLabel = C.findRootOfCluster(currentPoint->ptrToLabel);
#pragma omp parallel for num_threads(numOfThreads) private(listToBeSearched)
		for(int i = yStart; i <= yEnd; i++) // Go over each list in the vector dataPoints at index i. 	
		{
				listToBeSearched = (*dataPoints[i]);
				mergeWithAllNbrs(listToBeSearched, currentPoint, x, y, xStart, xEnd, newLabel);
				//		mergeWithOnlyLMAndRMNbrs(listToBeSearched, currentPoint, x, y, xStart, xEnd);
				//		processLeftMostNbrWithinThreshold(listToBeSearched, x, y, xStart, xEnd, currentPoint);
				//	processRightMostNbrWithinThreshold(listToBeSearched, x, y, xEnd, currentPoint);
		}
		
/*for(int i = 0; i < numOfThreads; i++)
		{
				set<Label*>::iterator itr;
				for(itr = labelsPerThread[i].begin(); itr != labelsPerThread[i].end(); ++itr)
				{
						if(newLabel->ptrToParentLabel != (*itr))
							C.mergeClusters(newLabel, *itr);
				}
		} */ 

/*#pragma omp parallel for num_threads(numOfThreads)
		for(int i = 0; i < numOfThreads; i++)
		{
				set<Label*>::iterator itr;
				for(itr = labelsPerThread[i].begin(); itr != labelsPerThread[i].end(); ++itr)
				{
						Label *otherLabel = *itr;
							otherLabel->ptrToParentLabel = newLabel;
				}
		} */

		/*Note: In every pair of labels for merging, one is the same across pairs - the label of the new point. The other one will be the root label of a neighbouring point. If we dont worry much about increasing the height of the label chains by one, then we can make the label of the new point the new root, and attach all the other labels as children to this. Note that this will remove the union-by-rank feature. IMPACT - Updating parent pointers of all these labels can now run in parallel - all threads write the same value in the parent pointer field of the labels they are handling. Also, no need to call findRoot in between, as the new root is the same for all the neighbour points. Current mergeClusters code checks again to see if the labels are really roots now (after some merges happened) and call findRoot again otherwise. */
		/*TODO - Is there a parallel reduction of multiple sets into one? - This can reduce the number of labels for which parent pointers are to be updated - i.e., avoid duplicates across different sets. Basically, we want a union of all these sets. */

	#pragma omp parallel for num_threads(numOfThreads)
		for(int i = 0; i < numOfThreads; i++)
		{
				vectorOfLabelsPerThread[i].clear();	
				vectorOfLabelsPerThread[i].insert(vectorOfLabelsPerThread[i].end(), labelsPerThread[i].begin(), labelsPerThread[i].end());
				for(int j = 0; j < vectorOfLabelsPerThread[i].size(); j++)
							{
									Label *otherLabel = vectorOfLabelsPerThread[i][j];
									otherLabel->ptrToParentLabel = newLabel;
							}
		}
/*#pragma omp parallel for num_threads(numOfThreads)
				 for(int i = 0; i < numOfThreads; i++)
					{
							for(int j = 0; j < vectorOfLabelsPerThread[i].size(); j++)
							{
									Label *otherLabel = vectorOfLabelsPerThread[i][j];
									otherLabel->ptrToParentLabel = newLabel;
							}
					} */

/*		Label* firstNbrLabel = NULL;
		for(int i = 0; i < numOfThreads; i++)
		{
			for(int j = 0; j < vectorOfLabelsPerThread[i].size(); j++)
			{
				firstNbrLabel = vectorOfLabelsPerThread[i][j];
				newLabel->ptrToParentLabel = firstNbrLabel;
				break;
			}
			if(firstNbrLabel != NULL)
					break;
		}

		 if(firstNbrLabel != NULL) // Some merge is to happen
		 {
#pragma omp parallel for num_threads(numOfThreads)
				 for(int i = 0; i < numOfThreads; i++)
					{
							for(int j = 0; j < vectorOfLabelsPerThread[i].size(); j++)
							{
									Label *otherLabel = vectorOfLabelsPerThread[i][j];
									if(otherLabel != firstNbrLabel)
											otherLabel->ptrToParentLabel = firstNbrLabel;
							}
					}
		 }
*/

//		set<Label*> unionOfOtherLabels;
/*		Label* firstNbrLabel = NULL;
		for(int threadNum = 0; threadNum < numOfThreads; threadNum++)
		{
				set<Label*>::iterator itr;
				for(itr = labelsPerThread[threadNum].begin(); itr != labelsPerThread[threadNum].end(); ++itr)
				{
						firstNbrLabel = *itr;
						newLabel->ptrToParentLabel = firstNbrLabel;
						break;
				}
				if(firstNbrLabel != NULL)
						break;
		} 

		if(firstNbrLabel != NULL) // Some merge is to happen
		{
#pragma omp parallel for num_threads(numOfThreads)
		for(int i = 0; i < numOfThreads; i++)
		{
				set<Label*>::iterator itr;
				for(itr = labelsPerThread[i].begin(); itr != labelsPerThread[i].end(); ++itr)
				{
						Label *otherLabel = *itr;
						if(otherLabel != firstNbrLabel)
						{
							otherLabel->ptrToParentLabel = firstNbrLabel;
						}
				}
		}
		} */ 

}

void addPointToExistingClusters(int vectorIndex, Node* currentPoint)
{
		insertStart = std::chrono::steady_clock::now();
		insertToDS(vectorIndex, currentPoint);
		insertEnd = std::chrono::steady_clock::now();
		insertToDStime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(insertEnd - insertStart).count()) / 1000.0; // in milli seconds
		mergeStart = std::chrono::steady_clock::now();
		mergeLabelOfNewlyAddedPoint(currentPoint);
		mergeEnd = std::chrono::steady_clock::now();
		mergeTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(mergeEnd - mergeStart).count()) / 1000.0; // in milli seconds
}

int main(int argc, char* argv[])
{
		if(argc != 5)
		{
				cout << "Usage: " << argv[0] << " <INPUT_FILE_NAME> <THRESHOLD_DISTANCE> <NAME_OF_FILE_WITH_DYN_DATA> <NUM_THREADS>" << endl;
				exit(0);
		}
		ROOT_DIR = getenv("CLUSTERING_ROOT");
		totalExecTime = 0.0;
		e = std::chrono::steady_clock::now();
		fileName = argv[1];
		thresholdDistance = atoi(argv[2]);
		string dynDataFileName;
		resultFileName = argv[3];
		resultFileName = resultFileName.substr(4); // Removes "dyn_" and keeps the remaining part of the dynamic input file name. Sample dyn file name: dyn_pokec_1pc.txt
		dynDataFileName.append(ROOT_DIR).append("/inputs/").append(argv[3]);
		numOfThreads = atoi(argv[4]);
		string inFilename;
		inFilename.append(ROOT_DIR).append("/inputs/").append(fileName);
		int x, y, label;
		Node* currentPoint;
		fstream inputFile;
		inputFile.open(inFilename, ios_base::in);
		if(!(inputFile.is_open()))
		{
				cout << "Could not open the file " << inFilename << ".. Exiting program..." << endl;
				exit(0);
		}
		/* Read the coordinates of the input points  */
		inputReadTime = 0.0;
		writeOutTime = 0.0;
		c = std::chrono::steady_clock::now();
		inputFile >> n; // Reading the number of points in this input file into global variable n.
		inputFile >> max_X;
		inputFile >> max_Y;
		// n points are there in the input file. And we are going to add 1000 points. But these 1000 points will have max values of X and Y coordinates max_X and max_Y respectively. So the vector size is reserved accordingly. 
		dataPoints.reserve(max_Y + 1); /* +1 so that indexing from 1 can be used */
		vectorOfLabelsPerThread.reserve(numOfThreads);
		vectorOfLabelsPerThread.resize(numOfThreads);

		set<pair<int, int>> allPoints;
		set<int> uniqueLabels;
		// remember to deallocate these vectors
		for(int i = 1; i <= max_Y; i++)
		{
				dataPoints[i] = new vector<Node*>;
		}
		//labelPairsPerThread.reserve(numOfThreads);
		//labelPairsPerThread.resize(numOfThreads);
		labelsPerThread.reserve(numOfThreads);
		labelsPerThread.resize(numOfThreads);
		while(true)
		{
				if(!(inputFile >> x))
						break;
				inputFile >> y;
				inputFile >> label;
				numOfPoints++;
				currentPoint = C.makeSingleElemCluster(x, y, label);
				(*dataPoints[y]).push_back(currentPoint);
				allPoints.insert(make_pair(x, y));
				uniqueLabels.insert(label);
		}
		// The vectors are already in sorted order. - ASSUMPTION
		d = std::chrono::steady_clock::now();
		inputReadTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(d - c).count()) / 1000.0; // in milli seconds
		inputFile.close();

		/* Read the new (not already existing) points from a file and add them to the clusters dynamically one at a time. ie., insert them in the right place in the data structure. Measure the cumulative time for each addition. */	
		int initialLabel;
		dynClusTime = 0.0;
		insertToDStime = 0.0;
		mergeTime = 0.0;
		fstream dynInputFile;
		dynInputFile.open(dynDataFileName, ios_base::in);
		if(!(dynInputFile.is_open()))
		{
				cout << "Could not open the file " << dynDataFileName << ".. Exiting program..." << endl;
				exit(0);
		}
		int addOrDel; // 1 => add, 0 => del
		while(true)
		{
				if(!(dynInputFile >> x))
						break;
				dynInputFile >> y;
				dynInputFile >> addOrDel;
				if(addOrDel == 1) // Add point
				{
						initialLabel = (*(uniqueLabels.rbegin())) + 1; // Value of the last element in sorted order + 1
						uniqueLabels.insert(initialLabel);
						currentPoint = C.makeSingleElemCluster(x, y, initialLabel);
						//	a = std::chrono::steady_clock::now();
						addPointToExistingClusters(y, currentPoint);
						//		b = std::chrono::steady_clock::now();
						//			dynClusTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(b - a).count()) / 1000.0; // in milli seconds
						numOfPoints++;
				}
		}
		dynClusTime = insertToDStime + mergeTime; // This line is executed only once

		writeOutput();
		writeOutTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(d - c).count()) / 1000.0; // in milli seconds
		outFile.close();

		writeInputForStaticClus();

		for(int i = 1; i <= max_Y; i++)
		{
				delete dataPoints[i];
		}
		dynInputFile.close();
		f = std::chrono::steady_clock::now();
		totalExecTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(f - e).count()) / 1000.0; // in milli seconds
		cout << numOfPoints << "\t" << dynClusTime / 1000.0 << "\t" << finalNumOfClusters <<  "\t" << numOfThreads << "\t\t" << (insertToDStime / dynClusTime) * 100.0 << "\t\t" <<  (mergeTime / dynClusTime) * 100.0 << "\t" << totalExecTime / 1000.0  << "\t" << dynDataFileName <<  endl;
		return 0;
}
