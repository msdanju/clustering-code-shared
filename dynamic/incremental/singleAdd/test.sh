#!/bin/bash
export OMP_PROC_BIND=true
mkdir -p ./stats

seqMergeFile="./stats/seqMerge_skew_0_test.txt"
parMergeFile="./stats/parMerge_skew_0_test.txt"
rm -f $parMergeFile
#rm -f $seqMergeFile

for i in {1,2,4,8,16,20,32}
do
#	./parSingleAdd.out skewness_0.txt 2000 aa.txt $i >> $seqMergeFile
	./parMergeSingleAdd.out skewness_0.txt 2000 aa.txt $i >> $parMergeFile
done

paste <(awk '{print $4, $2}' $seqMergeFile) <(awk '{print $2}' $parMergeFile)
