set terminal pdf enhanced
set output 'avg_comparingDiffParallelLoops.pdf'

set style data histogram
set style histogram cluster gap 1
set xlabel 'Inputs to which 15L points are added'
set ylabel 'Time taken (in minutes)'
set style fill solid border rgb "black"
set auto x
set yrange [0:*]
plot filename using 2:xtic(1) title 'Outer parallel', \
        '' using 3:xtic(1) title 'Inner parallel', \
        '' using 4:xtic(1) title 'Both parallel'
