set terminal postscript eps enhanced color font 'Helvetica,16'
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 2 \
    pointtype 7 pointsize 0.2
#set style line 2 \
#    linecolor rgb '#dd181f' \
#    linetype 1 linewidth 2 \
#    pointtype 5 pointsize 1.5
set autoscale
unset log
unset label
set xtic auto
set ytic auto
#set title 'Effect of number of threads on fully dynamic operations'
set xlabel 'Various degree values'
set logscale y 10
set ylabel 'Number of points with each degree'
#set xr [1025000:1525000]
#set yr [1.0:45.0]
set output 'dd_thresh_200_lj.eps'
plot filename using 1:2  with linespoints linestyle 1 notitle
#plot filename using 1:2  with points pointtype 7 pointsize 0.25 notitle
