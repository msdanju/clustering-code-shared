set terminal postscript eps enhanced color font 'Helvetica,16'
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 2 \
    pointtype 7 pointsize 1.5
#set style line 2 \
#    linecolor rgb '#dd181f' \
#    linetype 1 linewidth 2 \
#    pointtype 5 pointsize 1.5
set autoscale
unset log
unset label
set xtic auto
set ytic auto
#set title 'Effect of batch size on dynamic deletions'
set xlabel 'Batch size'
set ylabel 'Time taken (in minutes)'
#set xr [0:30]
set yr [0:12]
set output 'avg_effectOfBSonDeletions.eps'
plot filename using 1:2  with linespoints linestyle 1 notitle
