set terminal postscript eps enhanced color font 'Helvetica,16'
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 2 \
    pointtype 7 pointsize 1.5
#set style line 2 \
#    linecolor rgb '#dd181f' \
#    linetype 1 linewidth 2 \
#    pointtype 5 pointsize 1.5
set autoscale
unset log
unset label
set xtic auto
set ytic auto
set y2tics auto
set ytics nomirror
set y2tics format "%.1f"
#set xtics mirror
set xlabel 'Threshold'
set ylabel 'Execution time (in minutes)'
set y2label 'Error (in %)'

#set title 'Effect of threshold on dynamic deletions'
#set xr [0:30]
#set yr [15:35]
set output 'effectOfDeltaOnDeletions.eps'
plot filename using 1:2  with linespoints linestyle 1 title 'Time' axis x1y1,\
  filename using 1:3  with linespoints linestyle 2 title 'Error' axis x1y2

