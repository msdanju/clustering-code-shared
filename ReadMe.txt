1. Set bash environment variable CLUSTERING_ROOT to the path from / to the top directory of the application code.
e.g.: CLUSTERING_ROOT=/home/anju/clustering 

2. Inputs for static clustering should be in the folder clustering/static/inputsForStaticClustering

3. Copy the output of static clustering from clustering/static/results to clustering/inputs, which will be used as input for DYNAMIC - Note that the input for dynamic clustering should have x,y,label in each line

4. To generate synthetic data with specific skewness for static clustering, use the file skewedInputGen.cpp inside clustering/static/inputsForStaticClustering

5. To generate dynamic data for deletion or addition, use the labelled data available in clustering/inputs. The files genDataForAddition.cpp and genDataForDeletion.cpp inside clustering/datagen can be used for this purpose. Note that the input for these dynamic data generation files is expected to have x,y, label in each line.

6. The minimum value of x as well as y should be 1.

7. The input files are large in size. Hence only limited sample inputs are given here. Please contact us, if you need further details on the inputs.
