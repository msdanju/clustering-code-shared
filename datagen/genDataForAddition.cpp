#include <iostream>
#include <random>
#include <fstream>
#include <set>
#include <chrono>
#include <stdlib.h>
using namespace std;

int n, numExistingPoints;
ofstream outFile;
std::chrono::steady_clock::time_point a, b;
float inputGenTime;
int UPPER_BOUND;
int max_X, max_Y;
string ROOT_DIR;

int main(int argc, char* argv[])
{
	if(argc != 4)
	{
		cout << "Usage: " << argv[0] << " <NAME_OF_FILE_WITH_ORIGINAL_DATA> <NUM_ADDITIONAL_POINTS_TO_GEN> <NAME_OF_DYN_DATA_FILE>" << endl;
		exit(0);
	}
	ROOT_DIR = getenv("CLUSTERING_ROOT");
	a = std::chrono::steady_clock::now();
	string fileName = argv[1];
	string inFilename;
	inFilename.append(ROOT_DIR).append("/inputs/").append(fileName);
	fstream inputFile;
	inputFile.open(inFilename, ios_base::in);
	if(!(inputFile.is_open()))
	{
		cout << "Could not open original input file " << inFilename << ".. Exiting program..." << endl;
	}	
	n = atoi(argv[2]);
	string dynFileName = argv[3];
	inputFile >> numExistingPoints;
	inputFile >> max_X;
    	inputFile >> max_Y;

	int x, y, label;
	set<pair<int, int>> allPoints;
	while(true)
	{
		if(!(inputFile >> x))
			break;
		inputFile >> y;
		inputFile >> label; // Simply reading it, but we dont use it in this code
		allPoints.insert(make_pair(x, y));
	}
		
	mt19937 randGen;
	randGen.seed(random_device()());
	uniform_int_distribution<int> myDistributionForX(1, max_X);
	uniform_int_distribution<int> myDistributionForY(1, max_Y);
	
	string fname;
	fname.append(ROOT_DIR).append("/inputs/").append(dynFileName);
	outFile.open(fname, ios::trunc | ios::out);
//	outFile << n << "\t" << max_X << "\t" << max_Y << endl; // Writing the number of data points, max_X and max_Y into the first line

	while(allPoints.size() != numExistingPoints + n)
	{
		int sizeBeforeInsertion = allPoints.size();
		x = myDistributionForX(randGen);
		y = myDistributionForY(randGen);
		allPoints.insert(make_pair(x, y));
		if(sizeBeforeInsertion != allPoints.size())
			outFile << x << "\t" << y << "\t" << 1 << endl; // 1 indicates addition and 0 indicates deletion
	}
	outFile.close();
	b = std::chrono::steady_clock::now();
	inputGenTime = 0.0;
	inputGenTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(b - a).count()) / 1000.0; // in milli seconds
//	cout << "Input gen time for " << n << " points = " << inputGenTime / 1000.0 << " s = " << inputGenTime / 60000.0 << " min" << endl; 
}
