#include <iostream>
#include <random>
#include <fstream>
#include <set>
#include <vector>
#include <chrono>
#include <stdlib.h>
using namespace std;

int n, numExistingPoints;
ofstream outFile;
std::chrono::steady_clock::time_point a, b;
float inputGenTime;
int UPPER_BOUND;
int max_X, max_Y;
string ROOT_DIR;

int main(int argc, char* argv[])
{
		if(argc != 4)
		{
				cout << "Usage: " << argv[0] << " <NAME_OF_FILE_WITH_ORIGINAL_DATA> <NUM_POINTS_TO_BE_DEL> <NAME_OF_DYN_DATA_FILE>" << endl;
				exit(0);
		}
		ROOT_DIR = getenv("CLUSTERING_ROOT");
		a = std::chrono::steady_clock::now();
		string fileName = argv[1];
		string inFilename;
		inFilename.append(ROOT_DIR).append("/inputs/").append(fileName);
		fstream inputFile;
		inputFile.open(inFilename, ios_base::in);
		if(!(inputFile.is_open()))
		{
				cout << "Could not open original input file " << inFilename << ".. Exiting program..." << endl;
		}	
		n = atoi(argv[2]);
		string dynFileName = argv[3];
		inputFile >> numExistingPoints;
		inputFile >> max_X;
		inputFile >> max_Y;

		int x, y, label;
		vector<pair<int, int>> allPoints;
		vector<int> allX;
		vector<int> allY;
		allX.reserve(numExistingPoints + 1);
		allY.reserve(numExistingPoints + 1);
		int i = 1;
		while(true)
		{
				if(!(inputFile >> x))
						break;
				inputFile >> y;
				inputFile >> label; // Simply reading it, but we dont use it in this code
				allPoints.push_back(make_pair(x, y));
				allX[i] = x;
				allY[i] = y;
				i++;
		}

		mt19937 randGen;
		randGen.seed(random_device()());
		uniform_int_distribution<int> myDistributionForIndex(1, numExistingPoints);
		
		string fname;
		fname.append(ROOT_DIR).append("/inputs/").append(dynFileName);
		outFile.open(fname, ios::trunc | ios::out);
		//	outFile << n << "\t" << max_X << "\t" << max_Y << endl; // Writing the number of data points, max_X and max_Y into the first line
		set<int> indicesToBeDeleted;
		int count = 0, index;
		while(count < n)
		{
				index = myDistributionForIndex(randGen);
				int sizeBeforeInsertion = indicesToBeDeleted.size();
				indicesToBeDeleted.insert(index);
				if(indicesToBeDeleted.size() != sizeBeforeInsertion)
						count++;
		}

		set<int>::iterator itr;
		for(itr = indicesToBeDeleted.begin(); itr != indicesToBeDeleted.end(); ++itr)
		{
				index = *itr;
				x = allX[index];
				y = allY[index];
				outFile << x << "\t" << y << "\t" << 0 << endl; // 1 indicates addition and 0 indicates deletion	
		}

		outFile.close();
		b = std::chrono::steady_clock::now();
		inputGenTime = 0.0;
		inputGenTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(b - a).count()) / 1000.0; // in milli seconds
		//	cout << "Input gen time for " << n << " points = " << inputGenTime / 1000.0 << " s = " << inputGenTime / 60000.0 << " min" << endl; 
}
