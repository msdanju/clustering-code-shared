#include <iostream>
#include <random>
#include <fstream>
#include <set>
#include <chrono>
#include <stdlib.h>
using namespace std;

int n, numExistingPoints;
ofstream outFile;
std::chrono::steady_clock::time_point a, b;
float inputGenTime;
int UPPER_BOUND;
int max_X, max_Y;
string ROOT_DIR;

int main(int argc, char* argv[])
{
	if(argc != 4)
	{
		cout << "Usage: " << argv[0] << " <NAME_OF_FILE_WITH_ORIGINAL_DATA> <NUM_OF_DYN_OPS> <NAME_OF_DYN_DATA_FILE>" << endl;
		exit(0);
	}
	ROOT_DIR = getenv("CLUSTERING_ROOT");
	a = std::chrono::steady_clock::now();
	string fileName = argv[1];
	string inFilename;
	inFilename.append(ROOT_DIR).append("/inputs/").append(fileName);
	fstream inputFile;
	inputFile.open(inFilename, ios_base::in);
	if(!(inputFile.is_open()))
	{
		cout << "Could not open original input file " << inFilename << ".. Exiting program..." << endl;
	}	
	n = atoi(argv[2]);
	string dynFileName = argv[3];
	inputFile >> numExistingPoints;
	inputFile >> max_X;
    	inputFile >> max_Y;

	int x, y, label;
	set<pair<int, int>> allPoints;
	while(true)
	{
		if(!(inputFile >> x))
			break;
		inputFile >> y;
		inputFile >> label; // Simply reading it, but we dont use it in this code
		allPoints.insert(make_pair(x, y));
	}
	
	vector<pair<int,int>> initialPoints(allPoints.begin(), allPoints.end());
	
	mt19937 randGen;
	randGen.seed(random_device()());
	uniform_int_distribution<int> myDistributionForX(1, max_X);
	uniform_int_distribution<int> myDistributionForY(1, max_Y);
	uniform_int_distribution<int> distributionForOpSelect(1,100);

	string fname;
	fname.append(ROOT_DIR).append("/inputs/").append(dynFileName);
	outFile.open(fname, ios::trunc | ios::out);
//	outFile << n << "\t" << max_X << "\t" << max_Y << endl; // Writing the number of data points, max_X and max_Y into the first line

	int opSelect;
	int numOfAdditions = 0;
	int numOfDeletions = 0;
	vector<int> dynX, dynY, dynOp, dynDelIndices;
	dynX.reserve(n);
	dynX.resize(n);
	dynY.reserve(n);
	dynY.resize(n);
	dynOp.reserve(n);
	dynOp.resize(n);
	dynDelIndices.reserve(n);
	dynDelIndices.resize(n);

	for(int counter = 0; counter < n; counter++)
	{
		opSelect = distributionForOpSelect(randGen);
		if(opSelect <= 80) // generate a point for addition
		{
				int sizeBeforeInsertion = allPoints.size();
				while(true)
				{
						x = myDistributionForX(randGen);
						y = myDistributionForY(randGen);
						allPoints.insert(make_pair(x, y));
						if(sizeBeforeInsertion != allPoints.size())
						{
	//							outFile << x << "\t" << y << "\t" << 1 << endl; // 1 indicates addition and 0 indicates deletion
								dynX[counter] = x;
								dynY[counter] = y;
								dynOp[counter] = 1;
								numOfAdditions++;
								break; // insert successful, one point added
						}
				}
		}
		else // choose a point for deletion from the input file
		{
			/*vector<pair<int,int>> temp(allPoints.begin(), allPoints.end());
			uniform_int_distribution<int> myDistributionForIndex(0, temp.size() - 1);
			int index = myDistributionForIndex(randGen);
			x = temp[index].first;
			y = temp[index].second;
			outFile << x << "\t" << y << "\t" << 0 << endl; // 1 indicates addition and 0 indicates deletion			
			set<pair<int,int>>::iterator setItr;
			setItr = allPoints.find(make_pair(x,y));
			allPoints.erase(setItr);*/
			dynDelIndices[numOfDeletions] = counter;
      dynOp[counter] = 0;
			numOfDeletions++;
		}
	}

// Selecting points for deletion
	set<int> indicesToBeDeleted;
	uniform_int_distribution<int> myDistributionForIndex(1, numExistingPoints);
	int index;
	int count = 0;
	while(count < numOfDeletions)
	{
		index = myDistributionForIndex(randGen);
        int sizeBeforeInsertion = indicesToBeDeleted.size();
        indicesToBeDeleted.insert(index);
        if(indicesToBeDeleted.size() != sizeBeforeInsertion)
            count++;
	}

set<int>::iterator itr;
		int j = 0;
    for(itr = indicesToBeDeleted.begin(); itr != indicesToBeDeleted.end(); ++itr)
    {
			index = *itr;
			x = initialPoints[index].first;
			y = initialPoints[index].second;
			int dynIndex = dynDelIndices[j];
			j++;
			dynX[dynIndex] = x;
			dynY[dynIndex] = y;
		}

		for(int i = 0; i < (numOfAdditions + numOfDeletions); i++)
		{
			outFile << dynX[i] << "\t" << dynY[i] << "\t" << dynOp[i] << endl;	
		}

	outFile.close();
	b = std::chrono::steady_clock::now();
	inputGenTime = 0.0;
	inputGenTime += (float)(std::chrono::duration_cast<std::chrono::microseconds>(b - a).count()) / 1000.0; // in milli seconds
	cout << "Input gen time for " << n << " points = " << inputGenTime / 1000.0 << " s = " << inputGenTime / 60000.0 << " min" << endl; 
	cout << "additions = " << numOfAdditions << "   deletions = " << numOfDeletions  << "  total = " << numOfAdditions + numOfDeletions  << endl; 
}
