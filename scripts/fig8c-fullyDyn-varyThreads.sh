#!/bin/bash
export OMP_PROC_BIND=true

numRepetitions=5
inFile="pokec_original_thresh_2k.txt"
threshold=2000
dynDataFile="fd_pokec.txt"
batchSize=1000
exptName="fig8c-fullyDyn-VaryThreads"

cd ${CLUSTERING_ROOT}/dynamic/fullyDynamic/batchMode
make parBatchOp.out
mkdir -p ${CLUSTERING_ROOT}/results/${exptName}
rm -f ${CLUSTERING_ROOT}/results/${exptName}/*.txt # Clearing all files for a fresh execution of the experiment

# i stands for the number of threads used
for i in {1,2,4,8,16,20,32,40}
do
	rm -f temp.txt
  for ((j=1; j <=$numRepetitions; j++))
  do
		./parBatchOp.out $inFile $threshold $dynDataFile $i $batchSize >> temp.txt
	done
	echo "------------" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $i >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  cat temp.txt >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo "************" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $i > varyingParam.txt
  awk '{print $2/60}' temp.txt | sort -n > sorted.txt
  paste <(awk '{print $1}' varyingParam.txt) <(awk '{ total += $2/60; count++ } END { print total/count }' temp.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-avg.txt

  paste <(awk '{print $1}' varyingParam.txt) <(awk '{printf "%s" (FS), $1}' sorted.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-whisker.txt
  # already converted data to minutes
done

# effectOfNumThreadsOnFD.p

