#!/bin/sh
export OMP_PROC_BIND=true
if [ "$#" -ne 2 ]; then
	echo "Usage: $0 <INFILE> <THRESHOLD>"  >&2
	exit 1
fi

# collecting command line params
inFile=$1
threshold=$2
numThreads=20
dynDataFile=delFromOriginal_dyn_10_pokec.txt # Note the hardcoding of file name here

cd ${CLUSTERING_ROOT}/dynamic/decremental/batchDel/marking

make parBatchDelMark.out

mkdir -p ./stats

parFile="./stats/effectOfBS_onDel_thresh_${threshold}_threads_${numThreads}_${inFile}"

rm -f  $parFile

for i in {10,20,40,80,160,320,640,1280,2560,3000}
do
		batchSize=$i
		./parBatchDelMark.out $inFile $threshold $dynDataFile $numThreads 0 $batchSize  >> $parFile
done

echo "done"

awk '{print $7, $2/60}' $parFile > data.txt 
cp data.txt ${CLUSTERING_ROOT}/plots/effectOfBS_onDel_thresh_${threshold}_threads_${numThreads}_${inFile}

rm -f data.txt

cd ${CLUSTERING_ROOT}/plots

gnuplot -e "filename='effectOfBS_onDel_thresh_${threshold}_threads_${numThreads}_${inFile}'" effectOfBatchSizeOnDeletions.p

