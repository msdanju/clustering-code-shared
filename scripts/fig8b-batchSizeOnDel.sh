#!/bin/bash
export OMP_PROC_BIND=true

numRepetitions=5
inFile="pokec_original_thresh_200.txt"
threshold=200
numThreads=20
dynDataFile=delFromOriginal_dyn_10_pokec.txt # Note the hardcoding of file name here
exptName="fig8b-vary-batchSize-in-Del"

cd ${CLUSTERING_ROOT}/dynamic/decremental/batchDel/marking
make parBatchDelMark.out
mkdir -p ${CLUSTERING_ROOT}/results/${exptName}
rm -f ${CLUSTERING_ROOT}/results/${exptName}/*.txt # Clearing all files for a fresh execution of the experiment


for i in {16,32,64,128,256,512,1024,2048,4096}
do
	batchSize=$i
	rm -f temp.txt
	for ((j=1; j <=$numRepetitions; j++))
	do
	./parBatchDelMark.out $inFile $threshold $dynDataFile $numThreads 0 $batchSize  >> temp.txt
	done
	echo "------------" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $i >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  cat temp.txt >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo "************" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $i > varyingParam.txt
  awk '{print $2/60}' temp.txt | sort -n > sorted.txt
  paste <(awk '{print $1}' varyingParam.txt) <(awk '{ total += $2/60; count++ } END { print total/count }' temp.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-avg.txt

  paste <(awk '{print $1}' varyingParam.txt) <(awk '{printf "%s" (FS), $1}' sorted.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-whisker.txt
  # already converted data to minutes
done

#effectOfBatchSizeOnDeletions.p

