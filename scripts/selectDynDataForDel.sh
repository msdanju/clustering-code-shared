#!/bin/bash
export OMP_PROC_BIND=true
if [ "$#" -ne 2 ]; then
	echo "Usage: $0 <INFILE> <NUM_EXISTING_POINTS>" >&2
	exit 1
fi
inFile=$1
numExistingPoints=$2

cd ${CLUSTERING_ROOT}/datagen

make genDel.out 

chunkSize=$((1 * numExistingPoints / 100))

for i in {1..10..1}
do
		n=$((i * chunkSize))
		./genDel.out $inFile $n delFromOriginal_dyn_${i}_$inFile
done

