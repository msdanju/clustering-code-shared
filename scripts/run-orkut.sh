#!/bin/sh
export OMP_PROC_BIND=true

./static.sh orkut.txt 2000 
cp ${CLUSTERING_ROOT}/static/results/orkut.txt ${CLUSTERING_ROOT}/inputs/
#datagen

cd ${CLUSTERING_ROOT}/datagen
make genAdd.out
./genAdd.out orkut.txt 1500000 add_dyn_15L_orkut.txt

cd ${CLUSTERING_ROOT}/scripts
./compare-innerOuterBothIncremental.sh dataOrkut.txt
echo "done!!"
