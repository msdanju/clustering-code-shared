#!/bin/bash
export OMP_PROC_BIND=true
paramFile=$1
numThreads=20

cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/outer
make parBatchAddOuter.out
mkdir -p ./stats
parFileOuter="./stats/outer.txt"
#rm -f $parFileOuter

cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/inner
make parBatchAddInner.out
mkdir -p ./stats
parFileInner="./stats/inner.txt"
#rm -f $parFileInner

cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/both
make parBatchAddBoth.out
mkdir -p ./stats
parFileBoth="./stats/both.txt"
#rm -f $parFileBoth ten_two_loops.txt

#Executing all versions on all inputs
cd ${CLUSTERING_ROOT}/scripts
while read -r line
do
  if [[ ${line:0:1} != "#" ]]
  then
		IFS=";" read -ra oneLine <<< "$line"
    inFile=${oneLine[0]}
    dynDataFile=${oneLine[1]}
    threshold=${oneLine[2]}
    batchSize=${oneLine[3]}		

		cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/outer
		./parBatchAddOuter.out $inFile $threshold $dynDataFile $numThreads $batchSize  >> $parFileOuter

		cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/inner
		./parBatchAddInner.out $inFile $threshold $dynDataFile $numThreads $batchSize  >> $parFileInner

		cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/both
		./parBatchAddBoth.out $inFile $threshold $dynDataFile 20 $batchSize 2 >> $parFileBoth
		./parBatchAddBoth.out $inFile $threshold $dynDataFile 10 $batchSize 2 >> ten_two_loops.txt
		
	fi
	cd ${CLUSTERING_ROOT}/scripts
done < $paramFile

#Plotting
cd ${CLUSTERING_ROOT}/plots
rm -f diffParallelLoops.txt

paste <(awk '{print $8, $2/60}' ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/outer/stats/outer.txt) <(awk '{print $2/60}' ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/inner/stats/inner.txt) <(awk '{print $2/60}' ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/both/stats/both.txt) > diffParallelLoops.txt

gnuplot -e "filename='diffParallelLoops.txt'" barPlotForDiffLoopsParallel.p 


