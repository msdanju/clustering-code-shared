#!/bin/bash
export OMP_PROC_BIND=true

numRepetitions=5
inFile="thresh_2k_lj.txt"
threshold=2000
batchSize=1000
numThreads=20
exptName="fig7a-vary-add-percent"

cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/outer

make parBatchAddOuter.out
mkdir -p ${CLUSTERING_ROOT}/results/${exptName}
rm -f ${CLUSTERING_ROOT}/results/${exptName}/*.txt # Clearing all files for a fresh execution of the experiment

# i stands for the percentage of additions
for i in {1..10..1}
do
	dynDataFile=add_dyn_${i}_lj.txt # note the hardcoding of name
	rm -f temp.txt
  for ((j=1; j <=$numRepetitions; j++))
  do
	./parBatchAddOuter.out $inFile $threshold $dynDataFile $numThreads $batchSize  >> temp.txt
	done
	echo "------------" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $i >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  cat temp.txt >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo "************" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $i > varyingParam.txt
	awk '{print $2/60}' temp.txt | sort -n > sorted.txt
  paste <(awk '{print $1}' varyingParam.txt) <(awk '{ total += $2/60; count++ } END { print total/count }' temp.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-avg.txt

  paste <(awk '{print $1}' varyingParam.txt) <(awk '{printf "%s" (FS), $1}' sorted.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-whisker.txt
  # already converted data to minutes
done

#effectOfInsertions.p


