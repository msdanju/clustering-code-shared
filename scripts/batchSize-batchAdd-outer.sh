#!/bin/sh
export OMP_PROC_BIND=true
if [ "$#" -ne 2 ]; then
	echo "Usage: $0 <INFILE> <THRESHOLD>"  >&2
	exit 1
fi

# collecting command line params
inFile=$1
threshold=$2
numThreads=20
dynDataFile=add_dyn_10_${inFile}

cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/outer

make parBatchAddOuter.out

mkdir -p ./stats

parFile="./stats/effectOfBatchSize_thresh_${threshold}_threads_${numThreads}_${inFile}"

#rm -f  $parFile

#for i in {10,20,40,80,160,320,640,1280,2560,3000}
#do
#		batchSize=$i
#		./parBatchAddOuter.out $inFile $threshold $dynDataFile $numThreads $batchSize  >> $parFile
#done

awk '{print $7, $2/60}' $parFile > data.txt 
cp data.txt ${CLUSTERING_ROOT}/plots/effectOfBatchSize_thresh_${threshold}_threads_${numThreads}_${inFile}

rm -f data.txt

cd ${CLUSTERING_ROOT}/plots

gnuplot -e "filename='effectOfBatchSize_thresh_${threshold}_threads_${numThreads}_${inFile}'" effectOfBatchSizeInAdditions.p

