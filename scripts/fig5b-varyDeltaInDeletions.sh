#!/bin/bash
export OMP_PROC_BIND=true
#./varyDeltaInDeletions.sh pokec.txt 20 1000

numRepetitions=5
inFile="pokec.txt"
numThreads=20
batchSize=1000
dynDataFile="delFromOriginal_dyn_10_pokec.txt" # Note the hardcoding of pokec in the name
exptName="fig5bVaryDeltaInDel"

cd ${CLUSTERING_ROOT}/dynamic/decremental/batchDel/marking

make parBatchDelMark.out 
mkdir -p ${CLUSTERING_ROOT}/results/${exptName}
rm -f ${CLUSTERING_ROOT}/results/${exptName}/*.txt # Clearing all files for a fresh execution of the experiment

for delta in {200,400,600,800,1000,1200,1400,1600,1800,2000}
do
	rm -f temp.txt
  for ((j=1; j <=$numRepetitions; j++))
  do
	./parBatchDelMark.out delta_${delta}_$inFile $delta $dynDataFile $numThreads 0 $batchSize  >> temp.txt
	done
  echo "------------" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $delta >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  cat temp.txt >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo "************" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $delta > varyingParam.txt
  awk '{print $2/60}' temp.txt | sort -n > sorted.txt
  paste <(awk '{print $1}' varyingParam.txt) <(awk '{ total += $2/60; count++ } END { print total/count }' temp.txt) <(awk '{ total1 += $10; count1++ } END { print total1/count1 }' temp.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-avg.txt

  paste <(awk '{print $1}' varyingParam.txt) <(awk '{printf "%s" (FS), $1}' sorted.txt) <(awk '{ total1 += $10; count1++ } END { print total1/count1 }' temp.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-whisker.txt
  # already converted data to minutes
done

