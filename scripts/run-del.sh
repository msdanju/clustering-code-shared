#!/bin/bash
export OMP_PROC_BIND=true


#./deletionsByMarking-effectofBatchSize.sh pokec.txt 2000
#./static.sh pokec_original_thresh_200.txt 200
#cp ${CLUSTERING_ROOT}/static/results/pokec_original_thresh_200.txt ${CLUSTERING_ROOT}/inputs/
#cd ${CLUSTERING_ROOT}/scripts
#./numOfDeletions-batchMarking.sh pokec_original_thresh_200.txt 200 1000


#cd ${CLUSTERING_ROOT}/scripts
#./static.sh pokec_original_thresh_2k.txt 2000
#cp ${CLUSTERING_ROOT}/static/results/pokec_original_thresh_2k.txt ${CLUSTERING_ROOT}/inputs/
#cd ${CLUSTERING_ROOT}/scripts
#./numOfDeletions-batchMarking.sh pokec_original_thresh_2k.txt 2000 1000

cd ${CLUSTERING_ROOT}/scripts
./fullyDynamic_varyThreads.sh pokec_original_thresh_2k.txt 2000 fd_pokec.txt
