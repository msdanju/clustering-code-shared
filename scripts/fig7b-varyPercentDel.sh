#!/bin/bash
export OMP_PROC_BIND=true

numRepetitions=5
inFile="pokec_original_thresh_200.txt"
threshold=200
batchSize=1000
numThreads=20
exptName="fig7b-varyPercentOfDel"

cd ${CLUSTERING_ROOT}/dynamic/decremental/batchDel/marking
make parBatchDelMark.out 

mkdir -p ${CLUSTERING_ROOT}/results/${exptName}
rm -f ${CLUSTERING_ROOT}/results/${exptName}/*.txt # Clearing all files for a fresh execution of the experiment

# i stands for the percentage of deletions
for i in {1..10..1}
do
	dynDataFile=delFromOriginal_dyn_${i}_pokec.txt # Note the hardcoding of pokec in the name
	rm -f temp.txt
	for ((j=1; j <=$numRepetitions; j++))
  do
	./parBatchDelMark.out $inFile $threshold $dynDataFile $numThreads 0 $batchSize  >> temp.txt
	done
	echo "------------" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $i >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  cat temp.txt >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo "************" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $i > varyingParam.txt
  awk '{print $2/60}' temp.txt | sort -n > sorted.txt
  awk '{print $9/60}' temp.txt | sort -n > sorted-static.txt
  paste <(awk '{print $1}' varyingParam.txt) <(awk '{ total += $2/60; count++ } END { print total/count }' temp.txt) <(awk '{ total1 += $9/60; count1++ } END { print total1/count1 }' temp.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-avg.txt

  paste <(awk '{print $1}' varyingParam.txt) <(awk '{printf "%s" (FS), $1}' sorted.txt) <(awk '{printf "%s" (FS), $1}' sorted-static.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-whisker.txt
  # already converted data to minutes
done

#effectOfDeletions.p

