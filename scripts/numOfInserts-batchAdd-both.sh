#!/bin/sh
export OMP_PROC_BIND=true
if [ "$#" -ne 3 ]; then
	echo "Usage: $0 <INFILE> <THRESHOLD> <BATCH_SIZE>"  >&2
	exit 1
fi

# collecting command line params
inFile=$1
threshold=$2
batchSize=$3
numThreads=20

cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/both

make parBatchAddBoth.out

mkdir -p ./stats

parFile="./stats/effectOfInsertions_thresh_${threshold}_bs_${batchSize}_threads_${numThreads}_2_${inFile}"

rm -f $parFile

for i in {1..10..1}
do
		dynDataFile=dyn_${i}_${inFile}
		./parBatchAddBoth.out $inFile $threshold $dynDataFile $numThreads $batchSize 2  >> $parFile
done
