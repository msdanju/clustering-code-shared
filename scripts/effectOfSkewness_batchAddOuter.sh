#!/bin/bash
export OMP_PROC_BIND=true
#if [ "$#" -ne 3 ]; then
#	echo "Usage: $0 <INFILE> <THRESHOLD>  <BATCH_SIZE>"  >&2
#	exit 1
#fi

# collecting command line params
#inFile=$1
threshold=2000
batchSize=1000
numThreads=20

#cd ${CLUSTERING_ROOT}/static
#make parStatic.out
#mkdir -p ./stats
#staticParFile=./stats/effectOfSkewness_thresh_${threshold}_bs_${batchSize}_threads_${numThreads}_static.txt
#rm -f staticParFile
#for i in {0..100..10}
#do
#		./parStatic.out skewness_$i.txt 2000 20 >> $staticParFile
#		cp ./results/skewness_$i.txt ${CLUSTERING_ROOT}/inputs/
#done

#cd ${CLUSTERING_ROOT}/datagen
#make genAdd.out

#for j in {0..100..10}
#do
#		./genAdd.out skewness_$j.txt 100000 dyn_add_skewness_$j.txt
#done


cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/outer

make parBatchAddOuter.out

mkdir -p ./stats

parFile="./stats/effectOfSkewness_thresh_${threshold}_bs_${batchSize}_threads_${numThreads}.txt"

rm -f  $parFile

for i in {0..100..10}
do
		for j in {1..10..1}
		do
				#rm -f tmp.txt
				./parBatchAddOuter.out skewness_$i.txt $threshold dyn_add_skewness_$i.txt $numThreads $batchSize  >> tmp_$i.txt
		done
		# write avg of col 1 and avg of col2 of tmp.txt to parFile
		awk '{ sum += $2 } END { if (NR > 0) print sum / NR }' tmp_$i.txt >> $parFile 
		#echo "\n" >> $parFile

done

rm -f out.txt data.txt
for i in {0..100..10}
do
		echo $i >> out.txt
done

paste <(awk '{print $1}' out.txt) <(awk '{print $1/60}' $parFile) > data.txt

cp data.txt ${CLUSTERING_ROOT}/plots/effectOfSkewnessOnAdd_thresh_${threshold}_bs_${batchSize}_threads_${numThreads}_${inFile}

rm -f out.txt data.txt

cd ${CLUSTERING_ROOT}/plots

gnuplot -e "filename='effectOfSkewnessOnAdd_thresh_${threshold}_bs_${batchSize}_threads_${numThreads}_${inFile}'" effectOfSkewnessOnAdditions.p 
