#!/bin/sh
export OMP_PROC_BIND=true

inFile="pokec_thresh_2k.txt"
threshold=2000
numThreads=20
dynDataFile=delFromOriginal_dyn_10_pokec.txt

cd ${CLUSTERING_ROOT}/dynamic/decremental/singleDel/approx
make parSingleDelByReadd.out
mkdir -p ./stats
parFile="./stats/effectOfDeletions_thresh_${threshold}_threads_${numThreads}_${inFile}"
rm -f  $parFile
./parSingleDelByReadd.out $inFile $threshold $dynDataFile $numThreads  >> $parFile

cd ${CLUSTERING_ROOT}/dynamic/decremental/singleDel/marking
make parSingleDelMark.out
mkdir -p ./stats
parFile="./stats/effectOfDeletions_thresh_${threshold}_threads_${numThreads}_${inFile}"
rm -f  $parFile
./parSingleDelMark.out $inFile $threshold $dynDataFile $numThreads 0 >> $parFile

cd ${CLUSTERING_ROOT}/static
make parStatic.out
mkdir -p ./stats
parStaticFile="./stats/thresh_${threshold}_threads_${numThreads}_newData_${inFile}"
rm -f $parStaticFile
./parStatic.out newData_${inFile} $threshold $numThreads >> $parStaticFile

