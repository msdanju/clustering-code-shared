#!/bin/sh
export OMP_PROC_BIND=true
if [ "$#" -ne 2 ]; then
	echo "Usage: $0 <INFILE> <THRESHOLD>"  >&2
	exit 1
fi

# collecting command line params
inFile=$1
threshold=$2
numThreads=20

cd ${CLUSTERING_ROOT}/dynamic/decremental/singleDel/marking

make parSingleDelMark.out

mkdir -p ./stats

parFile="./stats/effectOfDeletions_thresh_${threshold}_threads_${numThreads}_${inFile}"

rm -f  $parFile

for i in {1..10..1}
do
		dynDataFile=delFromOriginal_dyn_${i}_${inFile}
		./parSingleDelMark.out $inFile $threshold $dynDataFile $numThreads 0 >> $parFile
done

# Note that the last command line parameter has to be set after uncommenting the code for running static clustering after a certain number of marking for deletions.
