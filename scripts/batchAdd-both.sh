#!/bin/sh
export OMP_PROC_BIND=true
if [ "$#" -ne 4 ]; then
	echo "Usage: $0 <INFILE> <THRESHOLD> <DYN_DATA_FILE> <BATCH_SIZE>"  >&2
	exit 1
fi

# collecting command line params
inFile=$1
threshold=$2
dynDataFile=$3
batchSize=$4


cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/both

make seqBatchAddBoth.out
make parBatchAddBoth.out

mkdir -p ./stats

seqFile="./stats/seqBatchAddBoth_thresh_${threshold}_bs_${batchSize}_${inFile}"
parFile="./stats/parBatchAddBoth_thresh_${threshold}_bs_${batchSize}_${inFile}"

rm -f $seqFile $parFile

#./seqBatchAddBoth.out $inFile $threshold $dynDataFile 1 $batchSize 1  > $seqFile

./parBatchAddBoth.out $inFile $threshold $dynDataFile 4 $batchSize 4  >> $parFile
./parBatchAddBoth.out $inFile $threshold $dynDataFile 8 $batchSize 4  >> $parFile
./parBatchAddBoth.out $inFile $threshold $dynDataFile 4 $batchSize 8  >> $parFile
./parBatchAddBoth.out $inFile $threshold $dynDataFile 2 $batchSize 16  >> $parFile
./parBatchAddBoth.out  $inFile $threshold $dynDataFile 16 $batchSize 2  >> $parFile
./parBatchAddBoth.out $inFile $threshold $dynDataFile 2 $batchSize 20  >> $parFile
./parBatchAddBoth.out $inFile $threshold $dynDataFile 20 $batchSize 2  >> $parFile
