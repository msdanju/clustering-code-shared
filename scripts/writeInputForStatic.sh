#!/bin/sh
export OMP_PROC_BIND=true

cd ${CLUSTERING_ROOT}/dynamic/decremental/batchDel/marking
g++ -std=c++11 -fopenmp -O3 writeInputForStaticAfterRemoval.cpp /home/anju/clustering/common/BasicClusters.cpp -I/home/anju/clustering/common -o writeInputForStatic

for i in {1,2,3,4,5,6,7,8,9,10}
do
	./writeInputForStatic pokec_original_thresh_200.txt 200 delFromOriginal_dyn_${i}_pokec.txt 20 0 1000
done

