#!/bin/bash
export OMP_PROC_BIND=true

numRepetitions=5
numThreads=20
batchSize=1000
threshold=2000
exptName="fig4bStaticParallel"

cd ${CLUSTERING_ROOT}/dynamic/fullyDynamic/batchMode
make parBatchOp.out
mkdir -p ${CLUSTERING_ROOT}/results/${exptName}
rm -f ${CLUSTERING_ROOT}/results/${exptName}/*.txt # Clearing all files for a fresh execution of the experiment

for filename in {pokec.txt,orkut.txt,usaRoadNet.txt,lj.txt,patents.txt}
do
rm -f temp.txt
	for ((j=1; j <=$numRepetitions; j++))
	do
		./parBatchOp.out thresh_${threshold}_${filename} $threshold fd_${filename} $numThreads $batchSize >> temp.txt
	done
	echo "------------" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $filename >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
	cat temp.txt >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
	echo "************" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
	echo $filename > varyingParam.txt
	awk '{print $2/60}' temp.txt | sort -n > sorted-dyn.txt
	paste <(awk '{print $1}' varyingParam.txt) <(awk '{ total += $2/60; count++ } END { print total/count }' temp.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-avg-dyn.txt

	paste <(awk '{print $1}' varyingParam.txt) <(awk '{printf "%s" (FS), $1}' sorted-dyn.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-whisker-dyn.txt
  # already converted data to minutes

	awk '{print $11/60}' temp.txt | sort -n > sorted-static.txt
	paste <(awk '{print $1}' varyingParam.txt) <(awk '{ total += $11/60; count++ } END { print total/count }' temp.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-avg-static.txt

	paste <(awk '{print $1}' varyingParam.txt) <(awk '{printf "%s" (FS), $1}' sorted-static.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-whisker-static.txt
  # already converted data to minutes

done


