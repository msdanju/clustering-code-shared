#!/bin/bash
export OMP_PROC_BIND=true
if [ "$#" -ne 3 ]; then
	echo "Usage: $0 <INFILE> <THRESHOLD>  <BATCH_SIZE>"  >&2
	exit 1
fi

# collecting command line params
inFile=$1
threshold=$2
batchSize=$3
numThreads=20

cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/outer

make parBatchAddOuter.out

mkdir -p ./stats

parFile="./stats/effectOfInsertions_thresh_${threshold}_bs_${batchSize}_threads_${numThreads}_${inFile}"

#rm -f  $parFile

#for i in {1..10..1}
#do
#		dynDataFile=dyn_${i}_${inFile}
#		./parBatchAddOuter.out $inFile $threshold $dynDataFile $numThreads $batchSize  >> $parFile
#done
rm -f out.txt data.txt
for i in {1..10..1}
do
		echo $i >> out.txt
done

paste <(awk '{print $1}' out.txt) <(awk '{print $2/60}' $parFile) > data.txt

cp data.txt ${CLUSTERING_ROOT}/plots/effectOfInsertions_thresh_${threshold}_bs_${batchSize}_threads_${numThreads}_${inFile}

rm -f out.txt data.txt

cd ${CLUSTERING_ROOT}/plots

gnuplot -e "filename='effectOfInsertions_thresh_${threshold}_bs_${batchSize}_threads_${numThreads}_${inFile}'" effectOfInsertions.p 
