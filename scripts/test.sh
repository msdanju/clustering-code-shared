#!/bin/sh
export OMP_PROC_BIND=true

if [ "$#" -ne 2 ]; then
	echo "Usage: $0 <INFILE> <THRESHOLD>" >&2
	exit 1
fi

inFile=$1
threshold=$2

cd ${CLUSTERING_ROOT}/static

make seqStatic.out
make parStatic.out

mkdir -p ./stats

seqFile="./stats/seqStatic_thresh_${threshold}_${inFile}"
parFile="./stats/parStatic_thresh_${threshold}_${inFile}"

rm -f $seqFile $parFile

echo "./seqStatic.out $inFile $threshold 1" > $seqFile

for i in {8,16,20,32,40}
do
		echo "./parStatic.out $inFile $threshold $i" >> $parFile
done

