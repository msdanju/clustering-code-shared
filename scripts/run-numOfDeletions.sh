#!/bin/sh
export OMP_PROC_BIND=true

#run static version with threshold 200
#./static.sh pokec.txt 200

#copy result of static version to clustering/inputs/
#cp ${CLUSTERING_ROOT}/static/results/pokec.txt ${CLUSTERING_ROOT}/inputs/

#run dynamic deletion by readditions
./numOfDeletions-readditions.sh pokec.txt 200

#run static clustering on the data after deletion
./run-static-on-newData.sh pokec.txt 200
