#!/bin/sh

./numOfInserts-singleAdd.sh pokec.txt 2000
./numOfInserts-batchAdd-outer.sh pokec.txt 2000 1000
./numOfInserts-batchAdd-inner.sh pokec.txt 2000 1000
./numOfInserts-batchAdd-both.sh pokec.txt 2000 1000

#TODO-Remove later.. Run only once..
cd ${CLUSTERING_ROOT}/static
make parStatic.out
mkdir -p ./stats
parFile="./stats/parStatic_thresh_2000_lj.txt"
./parStatic.out lj.txt 2000 20 >> $parFile
