#!/bin/sh
export OMP_PROC_BIND=true
if [ "$#" -ne 4 ]; then
	echo "Usage: $0 <INFILE> <THRESHOLD> <DYN_DATA_FILE> <BATCH_SIZE>"  >&2
	exit 1
fi

# collecting command line params
inFile=$1
threshold=$2
dynDataFile=$3
batchSize=$4


cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/inner

make seqBatchAddInner.out
make parBatchAddInner.out

mkdir -p ./stats

seqFile="./stats/seqBatchAddInner_thresh_${threshold}_bs_${batchSize}_${inFile}"
parFile="./stats/parBatchAddInner_thresh_${threshold}_bs_${batchSize}_${inFile}"

rm -f $seqFile $parFile

./seqBatchAddInner.out $inFile $threshold $dynDataFile 1 $batchSize  > $seqFile
for i in {8,16,20,32,40}
do
		./parBatchAddInner.out $inFile $threshold $dynDataFile $i $batchSize  >> $parFile
done

