#!/bin/bash
export OMP_PROC_BIND=true

numRepetitions=5
threshold=2000
batchSize=1000
numThreads=20
exptName="fig7c-skewnessOnAdd"

cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/outer
make parBatchAddOuter.out
mkdir -p ${CLUSTERING_ROOT}/results/${exptName}
rm -f ${CLUSTERING_ROOT}/results/${exptName}/*.txt # Clearing all files for a fresh execution of the experiment

# i stands for skewness
for i in {0..100..10}
do
	rm -f temp.txt
  for ((j=1; j <=$numRepetitions; j++))
  do
	./parBatchAddOuter.out skewness_$i.txt $threshold dyn_add_skewness_$i.txt $numThreads $batchSize  >> temp.txt
	done
	echo "------------" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $i >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  cat temp.txt >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo "************" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $i > varyingParam.txt
  awk '{print $2/60}' temp.txt | sort -n > sorted-minutes.txt
  paste <(awk '{print $1}' varyingParam.txt) <(awk '{ total += $2/60; count++ } END { print total/count }' temp.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-avg-minutes.txt

  paste <(awk '{print $1}' varyingParam.txt) <(awk '{printf "%s" (FS), $1}' sorted-minutes.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-whisker-minutes.txt
  # already converted data to minutes
	# collecting result in seconds
	awk '{print $2}' temp.txt | sort -n > sorted-seconds.txt
  paste <(awk '{print $1}' varyingParam.txt) <(awk '{ total += $2; count++ } END { print total/count }' temp.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-avg-seconds.txt

  paste <(awk '{print $1}' varyingParam.txt) <(awk '{printf "%s" (FS), $1}' sorted-seconds.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-whisker-seconds.txt

done

# effectOfSkewnessOnAdditions.p

