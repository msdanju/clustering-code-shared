#!/bin/sh
export OMP_PROC_BIND=true
if [ "$#" -ne 3 ]; then
	echo "Usage: $0 <INFILE> <THRESHOLD> <DYN_DATA_FILE>" >&2
	exit 1
fi
inFile=$1
threshold=$2
dynDataFile=$3

cd ${CLUSTERING_ROOT}/dynamic/incremental/singleAdd

make seqSingleAdd.out
make parSingleAdd.out

mkdir -p ./stats

seqFile="./stats/seqSingleAdd_thresh_${threshold}_${inFile}"
parFile="./stats/parSingleAdd_thresh_${threshold}_${inFile}"

rm -f $seqFile $parFile

./seqSingleAdd.out $inFile $threshold $dynDataFile  1 > $seqFile

for i in {8,16,20,32,40}
do
		./parSingleAdd.out $inFile $threshold $dynDataFile $i >> $parFile
done

#Added for testing. Remove this later..
cd ${CLUSTERING_ROOT}/static
make parStatic.out
mkdir -p ./stats
parStaticFile="./stats/parStatic_32_${inFile}"
rm -f $parStaticFile
./parStatic.out newData_${inFile} $threshold 32 >> $parStaticFile
