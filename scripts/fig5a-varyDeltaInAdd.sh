#!/bin/bash
export OMP_PROC_BIND=true

numRepetitions=5
inFile="pokec.txt"
numThreads=20
dynDataFile="add_dyn_15L_pokec.txt"
batchSize=1000
exptName="fig5aVaryDeltaInAdd"

cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/inner

make parBatchAddInner.out
mkdir -p ${CLUSTERING_ROOT}/results/${exptName}
rm -f ${CLUSTERING_ROOT}/results/${exptName}/*.txt # Clearing all files for a fresh execution of the experiment

for delta in {200,400,600,800,1000,1200,1400,1600,1800,2000}
do
	rm -f temp.txt
	for ((j=1; j <=$numRepetitions; j++))
	do
		./parBatchAddInner.out delta_${delta}_$inFile $delta $dynDataFile $numThreads $batchSize  >> temp.txt
	done
	echo "------------" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $delta >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  cat temp.txt >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo "************" >> ${CLUSTERING_ROOT}/results/${exptName}/all_runs.txt
  echo $delta > varyingParam.txt
  awk '{print $2/60}' temp.txt | sort -n > sorted.txt
  paste <(awk '{print $1}' varyingParam.txt) <(awk '{ total += $2/60; count++ } END { print total/count }' temp.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-avg.txt

  paste <(awk '{print $1}' varyingParam.txt) <(awk '{printf "%s" (FS), $1}' sorted.txt) >> ${CLUSTERING_ROOT}/results/${exptName}/output-whisker.txt
  # already converted data to minutes	
done

