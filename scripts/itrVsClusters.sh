#!/bin/sh
export OMP_PROC_BIND=true

#./static.sh thresh_2k_lj.txt 2000
#cp ${CLUSTERING_ROOT}/static/results/thresh_2k_lj.txt ${CLUSTERING_ROOT}/inputs/

cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/outer
./itrVsNumClust pokec_original_thresh_2k.txt 2000 add_dyn_1_pokec.txt 20 1000 > thresh_2k_pokec_batchNumberVsNumClusters.txt



#cd ${CLUSTERING_ROOT}/scripts
#./static.sh thresh_200_lj.txt 200
#cp ${CLUSTERING_ROOT}/static/results/thresh_200_lj.txt ${CLUSTERING_ROOT}/inputs/
#cd ${CLUSTERING_ROOT}/dynamic/incremental/batchAdd/outer
#./itrVsNumClust thresh_200_lj.txt 200 add_dyn_1_lj.txt 20 1000 > thresh_200_lj_batchNumberVsNumClusters.txt
