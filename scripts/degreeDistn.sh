#!/bin/sh
export OMP_PROC_BIND=true
if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <INFILE> <THRESHOLD>" >&2
  exit 1
fi
inFile=$1
threshold=$2
fileNameWithoutExt="$(echo $inFile | cut -d'.' -f1)"

cd ${CLUSTERING_ROOT}/featuresOfInput
make par_nbrDistribution.out

for i in {1,2,3,4,5,6,7,8,9,10}
do
		./par_nbrDistribution.out newData_thresh_${threshold}_delFromOriginal_dyn_${i}_${inFile} $threshold  20
		resultFile="results/nbrCount_thresh_${threshold}_newData_thresh_${threshold}_delFromOriginal_dyn_${i}_${inFile}"
		sort -n -k 1 $resultFile > sorted.txt
		cp sorted.txt ${CLUSTERING_ROOT}/plots/data_thresh_${threshold}_dyn_${i}.txt
		rm -f $resultFile sorted.txt
		cd ${CLUSTERING_ROOT}/plots
		gnuplot -e "filename='data_thresh_${threshold}_dyn_${i}.txt'" nbrDistribution.p
		mv nbrDist.eps nbrDist_thresh_${threshold}_dyn_${i}_${fileNameWithoutExt}.eps
		cd ${CLUSTERING_ROOT}/featuresOfInput
done

