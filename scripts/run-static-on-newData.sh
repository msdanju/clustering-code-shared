#!/bin/sh
export OMP_PROC_BIND=true
if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <INFILE> <THRESHOLD>"  >&2
  exit 1
fi

# collecting command line params
inFile=$1
threshold=$2
numThreads=20

cd ${CLUSTERING_ROOT}/static
make parStatic.out
mkdir -p ./stats
parStaticFile="./stats/thresh_${threshold}_threads_${numThreads}_newData_${inFile}"
rm -f $parStaticFile
#./parStatic.out newData_${inFile} $threshold $numThreads >> $parStaticFile

for i in {1..10..1}
do
   dynDataFile=delFromOriginal_dyn_${i}_${inFile}
   ./parStatic.out newData_${dynDataFile} $threshold $numThreads >> $parStaticFile
done
