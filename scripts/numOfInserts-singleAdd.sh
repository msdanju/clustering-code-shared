#!/bin/sh
export OMP_PROC_BIND=true
if [ "$#" -ne 2 ]; then
	echo "Usage: $0 <INFILE> <THRESHOLD>" >&2
	exit 1
fi
inFile=$1
threshold=$2
numThreads=20

cd ${CLUSTERING_ROOT}/dynamic/incremental/singleAdd

make parSingleAdd.out

mkdir -p ./stats

parFile="./stats/effectOfInsertions_thresh_${threshold}_threads_${numThreads}_${inFile}"

rm -f $parFile

for i in {1..10..1}
do
		dynDataFile=dyn_${i}_$inFile
		./parSingleAdd.out $inFile $threshold $dynDataFile $numThreads >> $parFile
done

#Added for testing. Remove this later..
cd ${CLUSTERING_ROOT}/static
make parStatic.out
mkdir -p ./stats
parStaticFile="./stats/parStatic_on_10_pc_points_${numThreads}_${inFile}"
rm -f $parStaticFile
./parStatic.out newData_${inFile} $threshold $numThreads >> $parStaticFile
