#!/bin/sh
export OMP_PROC_BIND=true
if [ "$#" -ne 2 ]; then
	echo "Usage: $0 <INFILE> <THRESHOLD>"  >&2
	exit 1
fi

# collecting command line params
inFile=$1
threshold=$2
numThreads=20

cd ${CLUSTERING_ROOT}/dynamic/decremental/singleDel/approx

make parSingleDelByReadd.out

mkdir -p ./stats

parFile="./stats/effectOfDeletions_thresh_${threshold}_threads_${numThreads}_${inFile}"

rm -f  $parFile

for i in {1..10..1}
do
		dynDataFile=delFromOriginal_dyn_${i}_${inFile}
		./parSingleDelByReadd.out $inFile $threshold $dynDataFile $numThreads  >> $parFile
done

