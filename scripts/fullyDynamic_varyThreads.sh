#!/bin/sh
export OMP_PROC_BIND=true
if [ "$#" -ne 3 ]; then
	echo "Usage: $0 <INFILE> <THRESHOLD> <DYN_DATA_FILE>" >&2
	exit 1
fi
inFile=$1
threshold=$2
dynDataFile=$3

cd ${CLUSTERING_ROOT}/dynamic/fullyDynamic

make parSingleOp.out

mkdir -p ./stats

parFile="./stats/fd_parSingleOp_thresh_${threshold}_${inFile}"

rm -f $parFile


for i in {4,8,16,20,32,40,2,1}
do
	./parSingleOp.out $inFile $threshold $dynDataFile $i >> $parFile
done

awk '{print $4, $2/60, $11}' $parFile > data.txt

cp data.txt ${CLUSTERING_ROOT}/plots/fd_parSingleOp_thresh_${threshold}_${inFile}

rm -f data.txt

cd ${CLUSTERING_ROOT}/plots

gnuplot -e "filename='fd_parSingleOp_thresh_${threshold}_${inFile}'" effectOfNumThreadsOnFD.p

