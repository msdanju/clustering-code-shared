#!/bin/bash
export OMP_PROC_BIND=true
if [ "$#" -ne 2 ]; then
	echo "Usage: $0 <INFILE> <NUM_EXISTING_POINTS>" >&2
	exit 1
fi
inFile=$1
numExistingPoints=$2

cd ${CLUSTERING_ROOT}/datagen

make genAdd.out

chunkSize=$((1 * numExistingPoints / 100))

for i in {1..10..1}
do
		n=$((i * chunkSize))
		./genAdd.out $inFile $n dyn_${i}_$inFile
done

