#include "BasicClusters.h"
#include <iostream>
using namespace std;

Node* BasicClusters::makeSingleElemCluster(int a, int b, int initialLabel)
{
		Node *newSet = new Node();
		newSet->x = a;
		newSet->y = b;
		// Creating a new label
		Label *newLabelPtr = new Label();
		if(newLabelPtr == NULL)
		{
				cout << "In makeSingleElemCluster fn.. new operator returned NULL.. Exiting.." << endl;
				exit(0);
		}
		newLabelPtr->labelVal = initialLabel;
		newLabelPtr->rank = 0;
		newLabelPtr->ptrToParentLabel = newLabelPtr; // make the label its own parent
		pair<set<Label*>::iterator, bool> ret;
		ret = allLabels.insert(newLabelPtr);
		if(ret.second == false)
		{
				// element with same labelVal exists. insert failed
				set<Label*>::iterator it = ret.first;
				newLabelPtr = *(it); // Reuse the existing Label pointer
				if(newLabelPtr == NULL)
				{
						cout << "newLabelPtr is null.. Exiting.." << endl;
						exit(0);
				}
		}
		newSet->ptrToLabel = newLabelPtr;
		newSet->isDeleted = false;
		return newSet;
}

/* Read only version. NO path compression. The function findRootOfCluster finds out the root of the tree containing the Label given in the argument. Once the root is found, the parent pointers of all the labels in the chain traversed are made to point to this root. */
Label* BasicClusters::findRootOfCluster(Label *ptrToLabel)
{
		if(ptrToLabel->ptrToParentLabel != ptrToLabel)
		{
				return findRootOfCluster(ptrToLabel->ptrToParentLabel);
		}
		else
		{
				return ptrToLabel->ptrToParentLabel;
		}
}


/* The function mergeClusters merges the clusters (that is, link their labels) containing the nodes given in the argument list. It attaches the root of the smaller tree to the root of the larger tree. If the trees are of the same height, then the rank of the final root is incremented by one.*/
void BasicClusters::mergeClusters(Label *parentLabel_a, Label *parentLabel_b)
{
		if(parentLabel_a->ptrToParentLabel != parentLabel_a)
				parentLabel_a = findRootOfCluster(parentLabel_a);
		if(parentLabel_b->ptrToParentLabel != parentLabel_b)
				parentLabel_b = findRootOfCluster(parentLabel_b);
		if(parentLabel_a == parentLabel_b)
				return;

		if(parentLabel_a->rank < parentLabel_b->rank)
		{
				parentLabel_a->ptrToParentLabel = parentLabel_b;
		}
		else
		{
				parentLabel_b->ptrToParentLabel = parentLabel_a;
				if(parentLabel_a->rank == parentLabel_b->rank)
						(parentLabel_a->rank)++;
		}
		/*  if((parentLabel_a->ptrToParentLabel == parentLabel_b) && parentLabel_b->ptrToParentLabel == parentLabel_a )
				{
				cout << "Loop detected.....Exiting..." << endl;
				exit(0);

				} */
}

/*The function deleteNode simply marks the Node as deleted, without actually deallocating memory. */
void BasicClusters::deleteNode(Node *a)
{
		a->isDeleted = true;
}

bool BasicClusters::isDeleted(Node *a)
{
		return a->isDeleted;
}



