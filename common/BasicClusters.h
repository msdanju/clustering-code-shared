#include <set>
using namespace std;
struct Node
{
		int x, y;
		bool isDeleted;
		struct Label *ptrToLabel;
};

struct Label
{
		int labelVal;
		int rank;
		struct Label *ptrToParentLabel;	
};

struct setComp
{
		bool operator()(const Label* a, const Label* b) const
		{
				return a->labelVal < b->labelVal;
		}
};

class BasicClusters
{
		protected:
				set<Label*> rootPtrs; // This is a set of root pointers of all the disjoint sets (or union-find trees) which represent the clusters.
		public:
				set<Label*, setComp> allLabels;
				Node* makeSingleElemCluster(int a, int b, int initialLabel);
				Label* findRootOfCluster(Label *ptrToData);
				void mergeClusters(Label *a, Label *b);
				void deleteNode(Node *a);
				bool isDeleted(Node *a);
};
